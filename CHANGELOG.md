# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v0.0.4...v0.0.5) (2024-09-10)


### Features

* **CRUD:** ✨ MODULO USERS ([8595b6a](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/8595b6aea059af437f88f8139384d04f95337b13))
* **Modulo:** ✨ MODULO USERS ([fc490bc](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/fc490bc64ad74513aec2e8770718436b328468af))


### Bug Fixes

* **Document:** 🐛 versionamiento ([d600acf](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/d600acf3e0b1f7e9944ff08498349be56a339927))

### [0.0.4](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v0.0.3...v0.0.4) (2024-06-21)

### [0.0.3](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v0.0.2...v0.0.3) (2024-06-21)

### [0.0.2](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v0.0.1...v0.0.2) (2024-06-21)

### [0.0.1](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.10...v0.0.1) (2024-06-21)

### Bug Fixes

-   **Document:** 🐛 husky version changelog ([e151e5f](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/e151e5f8ed26f9c8fda6c4ba80052a3e50dd6545))

### [1.3.10](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.9...v1.3.10) (2024-06-21)

### [1.3.9](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.8...v1.3.9) (2024-06-21)

### [1.3.8](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.7...v1.3.8) (2024-06-21)

### [1.3.7](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.6...v1.3.7) (2024-06-21)

### [1.3.6](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.5...v1.3.6) (2024-06-21)

### [1.3.5](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.4...v1.3.5) (2024-06-21)

### [1.3.4](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.3...v1.3.4) (2024-06-21)

### [1.3.3](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.0...v1.3.3) (2024-06-21)

### Bug Fixes

-   **Document:** 🐛 update version package.json con husky ([964fa53](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/964fa539daf259ef895fc0dd0d61865f1e7f3ca1))

### [1.3.2](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.1...v1.3.2) (2024-06-21)

### [1.3.1](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.3.0...v1.3.1) (2024-06-21)

## [1.3.0](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/compare/v1.2.0...v1.3.0) (2024-06-21)

### Features

-   **none:** ✨ configuracion de husky para automatizar el versionamiento automatico por cada push ([90de95f](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/90de95fcb1010681c30d76134f770d4a849a9068))

## 1.2.0 (2024-06-21)

### Features

-   **Document:** :memo: ([8ba2df2](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/8ba2df24d817a02a3c72909fe7a78768641ef3ec))

## <small>1.1.1 (2024-06-20)</small>

-   fixed some bugs ([72784d6](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/72784d6))
-   incio de sesion con google y menu modificado ([071a117](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/071a117))
-   inicio del proyecto! ([d0db118](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/d0db118))
-   Initial commit ([49aa5b1](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/49aa5b1))
-   modificaciones en el back, ahora las peticiones seran por guid y no por id ([24b1ed5](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/24b1ed5))
-   prueba de husky ([7eb954f](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/7eb954f))
-   se agrego la opcion de poder exportar en pdf y excel las tablas ([33d1bb8](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/33d1bb8))
-   se corregirieron unos parametros para adaptarse a los ajustes que se hicieron en el back para el ini ([84dc36b](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/84dc36b))
-   se hizo funcionar la tabla de usuarios con la api ([1575a4e](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/1575a4e))
-   se hizo implmento el nuevo refreshtoken de jwt que nos proporciona el back y asi poder crear token e ([f4c74c0](https://gitlab.com/ronaldo-carlin/adminforleagues-admin/commit/f4c74c0))
