import React, { createContext, useState, ReactNode, useContext } from 'react';
import { userClient } from '../services/users';
import { TUser } from '../types/users';

type UserContextType = {
  users: TUser[];
  fetchUsers: () => void;
  isModalAdd: boolean;
  setModalAdd: (value: boolean) => void;
};

const UserContext = createContext<UserContextType | undefined>(undefined);

export const UserProvider = ({ children }: { children: ReactNode }) => {
  const [users, setUsers] = useState<TUser[]>([]);
  const [isModalAdd, setModalAdd] = useState<boolean>(false);

  const fetchUsers = async () => {
    try {
      const response = await userClient.users();
      setUsers(response.data);
    } catch (error) {
      console.error('Error fetching users:', error);
    }
  };

  return <UserContext.Provider value={{ users, fetchUsers, isModalAdd, setModalAdd }}>{children}</UserContext.Provider>;
};

export const useUserContext = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUserContext must be used within a UserProvider');
  }
  return context;
};

export default UserContext;
