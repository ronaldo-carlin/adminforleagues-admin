// src/context/authContext.tsx
import React, { createContext, FC, ReactNode, useContext, useEffect, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { gapi } from 'gapi-script';
import useLocalStorage from '../hooks/useLocalStorage';
import { authPages } from '../config/pages.config';
import useUserAPI from '../hooks/useUserAPI';
import { TUser } from '../mocks/db/users.db';
import { el } from '@fullcalendar/core/internal-common';

export interface IAuthContextProps {
  usernameStorage: string | ((newValue: string | null) => void) | null;
  getToken: () => string | null;
  onLogin: (username: string, password: string) => Promise<void>;
  onLoginGoogle: () => Promise<void>;
  onLogout: () => void;
  userData: TUser | undefined;
  isLoading: boolean;
  isAuthenticated: boolean;
  tokenReady: Promise<void> | null;
}

const AuthContext = createContext<IAuthContextProps>({} as IAuthContextProps);

interface IAuthProviderProps {
  children: ReactNode;
}

export const AuthProvider: FC<IAuthProviderProps> = ({ children }) => {
  const [usernameStorage, setUserName] = useLocalStorage('user', null);
  const [refreshTokenStorage, setRefreshToken] = useLocalStorage('token', null);
  const [accessToken, setAccessToken] = useState<string | null>(null);
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [tokenReady, setTokenReady] = useState<Promise<void> | null>(null);
  const [isRenewingToken, setIsRenewingToken] = useState<boolean>(false);

  const { response, isLoading, getCheckUser, getcheckGoogle, renewAccessToken } = useUserAPI(usernameStorage as string);
  const userData = response;
  /* console.log('userData:', userData); */

  const navigate = useNavigate();

  // Función para manejar la renovación del token
  const handleTokenRenewal = async () => {
    if (isRenewingToken) return; // Evita la llamada si ya está en progreso

    setIsRenewingToken(true);

    if (refreshTokenStorage) {
      try {
        const response = await renewAccessToken(refreshTokenStorage as string);
        if (response.data.token) {
          setIsAuthenticated(true);
          setAccessToken(response.data.token);
          if (typeof setRefreshToken === 'function') {
            await setRefreshToken(response.data.refreshToken);
          }
          if (typeof setUserName === 'function') {
            await setUserName(response.data.user);
          }
          setTokenReady(Promise.resolve()); // Indica que el token está listo
        } else {
          console.error('Error fetching user data');
          onLogout();
        }
      } catch (error) {
        console.error('Error catch user data:', error);
        onLogout();
      }
    } else {
      console.log('No hay refresh token en el almacenamiento local.');
      onLogout();
    }

    setIsRenewingToken(false); // Marca que la renovación ha terminado
  };

  // Llamar la función de renovación del token al montar el AuthProvider
  useEffect(() => {
    const renewToken = async () => {
      await handleTokenRenewal();
    };

    renewToken();
  }, []); // Ahora depende de accessToken

  const onLogin = async (username: string, password: string) => {
    try {
      const response = await getCheckUser(username, password);
      if (response.data.token && typeof setUserName === 'function' && typeof setRefreshToken === 'function') {
        setAccessToken(response.data.token);
        await setRefreshToken(response.data.refreshToken);
        setIsAuthenticated(true);
        await setUserName(response.data.user).then(() => navigate('/'));
      } else {
        console.error('Error fetching user data:', response);
        return response.error;
      }
    } catch (error) {
      console.error('Error fetching user data:', error);
      return error;
    }
  };

  const onLoginGoogle = async () => {
    const googleAuth = gapi.auth2.getAuthInstance();
    const token = googleAuth.currentUser.get().getAuthResponse().id_token;
    await getcheckGoogle(token).then(async (response) => {
      if (response.data.token && typeof setUserName === 'function' && typeof setRefreshToken === 'function') {
        setAccessToken(response.data.token);
        await setRefreshToken(response.data.refreshToken);
        setIsAuthenticated(true);
        await setUserName(response.data.user).then(() => navigate('/'));
      }
    });
  };

  const onLogout = async () => {
    if (typeof setUserName === 'function' && typeof setRefreshToken === 'function') {
      setIsAuthenticated(false);
      await setUserName(null);
      await setRefreshToken(null);
      setAccessToken(null);
      window.localStorage.removeItem('user');
      window.localStorage.removeItem('token');
    }
    navigate(`../${authPages.loginPage.to}`, { replace: true });
  };

  const getToken = () => accessToken;

  const value: IAuthContextProps = useMemo(
    () => ({
      getToken,
      onLogin,
      onLoginGoogle,
      onLogout,
      userData,
      isLoading,
      usernameStorage,
      isAuthenticated,
      tokenReady,
    }),
    [usernameStorage, userData, accessToken, isAuthenticated, isLoading, tokenReady],
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => useContext(AuthContext);
