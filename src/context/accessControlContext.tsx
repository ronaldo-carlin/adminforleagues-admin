import React, { createContext, useState, useEffect, ReactNode, useContext } from 'react';
import { AccessControlClient } from '../services/access_control';
import { PermissionsClient } from '../services/permissions';
import { ModulesClient } from '../services/modules';
import { TAccessControls } from '../types/access_control';
import { TModules } from '../types/modules';
import { TPermissions } from '../types/permissions';
import { useAuth } from './authContext'; // Importa el AuthContext
import { use } from 'i18next';

type AccessControlContextType = {
  accessControlByRol: TAccessControls[];
  fetchAccessControlByRol: (guid: string) => void;
  accessControls: TAccessControls[];
  fetchAccessControls: () => void;
  modules: TModules[];
  fetchModules: () => void;
  permissions: TPermissions[];
  fetchPermissions: () => void;
};

const AccessControlContext = createContext<AccessControlContextType | undefined>(undefined);

export const AccessControlProvider = ({ children }: { children: ReactNode }) => {
  const { userData, getToken, tokenReady } = useAuth(); // Obtener el usuario autenticado
  const [accessControlByRol, setAccessControlByRol] = useState<TAccessControls[]>([]);
  const [accessControls, setAccessControls] = useState<TAccessControls[]>([]);
  const [modules, setModules] = useState<TModules[]>([]);
  const [permissions, setPermissions] = useState<TPermissions[]>([]);

  // función para obtener los accesos que tiene un rol
  const fetchAccessControlByRol = async (guid: string) => {
    try {
      const token = getToken(); // Obtén el token
      if (token) {
        // Verifica que el token esté listo
        const response = await AccessControlClient.AccessControlByRol({ guid: guid });
        setAccessControlByRol(response.data);
      }
    } catch (error) {
      console.error('Error fetching access controls:', error);
    }
  };

  // Funcion para obtener los accesos que tiene el rol del usuario autenticado
  useEffect(() => {
    // Solo realiza la petición cuando el token está listo
    const checkTokenAndFetch = async () => {
      await tokenReady; // Espera a que el token esté listo
      if (userData?.user?.role_guid && getToken()) {
        fetchAccessControlByRol(userData.user.role_guid);
      }
    };
    checkTokenAndFetch();
  }, [userData, tokenReady]);

  // función para obtener todos los accesos
  const fetchAccessControls = async () => {
    try {
      const token = getToken(); // Obtén el token
      if (token) {
        // Verifica que el token esté listo
        const response = await AccessControlClient.AccessControls();
        setAccessControls(response.data);
      }
    } catch (error) {
      console.error('Error fetching access controls:', error);
    }
  };

  // funcion para obtener los modulos
  const fetchModules = async () => {
    try {
      const token = getToken(); // Obtén el token
      if (token) {
        // Verifica que el token esté listo
        const response = await ModulesClient.Modules();
        setModules(response.data);
      }
    } catch (error) {
      console.error('Error fetching modules:', error);
    }
  };

  // funcion para oberner los permisos
  const fetchPermissions = async () => {
    try {
      const token = getToken(); // Obtén el token
      if (token) {
        // Verifica que el token esté listo
        const response = await PermissionsClient.Permissions();
        setPermissions(response.data);
      }
    } catch (error) {
      console.error('Error fetching permissions:', error);
    }
  };

  return (
    <AccessControlContext.Provider
      value={{
        accessControlByRol,
        fetchAccessControlByRol,
        accessControls,
        fetchAccessControls,
        modules,
        fetchModules,
        permissions,
        fetchPermissions,
      }}
    >
      {children}
    </AccessControlContext.Provider>
  );
};

export const useAccessControlContext = () => {
  const context = useContext(AccessControlContext);
  if (!context) {
    throw new Error('useAccessControlContext must be used within a AccessControlProvider');
  }
  return context;
};

export default AccessControlContext;
