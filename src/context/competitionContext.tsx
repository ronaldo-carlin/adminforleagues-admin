import React, { createContext, useState, ReactNode, useContext } from 'react';
import { competitionClient } from '../services/competitions';
import { TCompetition } from '../types/competitions';

type CompetitionContextType = {
  competitions: TCompetition[];
  fetchCompetitions: () => void;
  isModalAdd: boolean;
  setModalAdd: (value: boolean) => void;
};

const CompetitionContext = createContext<CompetitionContextType | undefined>(undefined);

export const CompetitionProvider = ({ children }: { children: ReactNode }) => {
  const [competitions, setCompetitions] = useState<TCompetition[]>([]);
  const [isModalAdd, setModalAdd] = useState<boolean>(false);

  const fetchCompetitions = async () => {
    try {
      const response = await competitionClient.Competitions();
      setCompetitions(response.data);
    } catch (error) {
      console.error('Error fetching competitions:', error);
    }
  };

  return <CompetitionContext.Provider value={{ competitions, fetchCompetitions, isModalAdd, setModalAdd }}>{children}</CompetitionContext.Provider>;
};

export const useCompetitionContext = () => {
  const context = useContext(CompetitionContext);
  if (!context) {
    throw new Error('useCompetitionContext must be used within a CompetitionProvider');
  }
  return context;
};

export default CompetitionContext;
