import React, { createContext, useState, ReactNode, useContext } from 'react';
import { sedeClient } from '../services/sedes';
import { TSede } from '../types/sedes';

type SedeContextType = {
  sedes: TSede[];
  fetchSedes: () => void;
  isModalAdd: boolean;
  setModalAdd: (value: boolean) => void;
};

const SedeContext = createContext<SedeContextType | undefined>(undefined);

export const SedeProvider = ({ children }: { children: ReactNode }) => {
  const [sedes, setSedes] = useState<TSede[]>([]);
  const [isModalAdd, setModalAdd] = useState<boolean>(false);

  const fetchSedes = async () => {
    try {
      console.log('Fetching sedes...');
      const response = await sedeClient.Sedes();
      console.log('Sedes:', response.data);
      setSedes(response.data);
    } catch (error) {
      console.error('Error fetching sedes:', error);
    }
  };

  return <SedeContext.Provider value={{ sedes, fetchSedes, isModalAdd, setModalAdd }}>{children}</SedeContext.Provider>;
};

export const useSedeContext = () => {
  const context = useContext(SedeContext);
  if (!context) {
    throw new Error('useSedeContext must be used within a SedeProvider');
  }
  return context;
};

export default SedeContext;
