import React, { createContext, useState, ReactNode, useContext } from 'react';
import { teamClient } from '../services/teams';
import { TTeam } from '../types/teams';

type TeamContextType = {
  teams: TTeam[];
  fetchTeams: () => void;
  isModalAdd: boolean;
  setModalAdd: (value: boolean) => void;
};

const TeamContext = createContext<TeamContextType | undefined>(undefined);

export const TeamProvider = ({ children }: { children: ReactNode }) => {
  const [teams, setTeams] = useState<TTeam[]>([]);
  const [isModalAdd, setModalAdd] = useState<boolean>(false);

  const fetchTeams = async () => {
    try {
      console.log('Fetching teams...');
      const response = await teamClient.Teams();
      console.log('Teams:', response.data);
      setTeams(response.data);
    } catch (error) {
      console.error('Error fetching teams:', error);
    }
  };

  return <TeamContext.Provider value={{ teams, fetchTeams, isModalAdd, setModalAdd }}>{children}</TeamContext.Provider>;
};

export const useTeamContext = () => {
  const context = useContext(TeamContext);
  if (!context) {
    throw new Error('useTeamContext must be used within a TeamProvider');
  }
  return context;
};

export default TeamContext;
