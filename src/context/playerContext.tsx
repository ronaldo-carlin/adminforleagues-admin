import React, { createContext, useState, ReactNode, useContext } from 'react';
import { playerClient } from '../services/players';
import { TPlayer } from '../types/players';

type PlayerContextType = {
  players: TPlayer[];
  fetchPlayers: () => void;
  isModalAdd: boolean;
  setModalAdd: (value: boolean) => void;
};

const PlayerContext = createContext<PlayerContextType | undefined>(undefined);

export const PlayerProvider = ({ children }: { children: ReactNode }) => {
  const [players, setPlayers] = useState<TPlayer[]>([]);
  const [isModalAdd, setModalAdd] = useState<boolean>(false);

  const fetchPlayers = async () => {
    try {
      console.log('Fetching players...');
      const response = await playerClient.Players();
      console.log('Players:', response.data);
      setPlayers(response.data);
    } catch (error) {
      console.error('Error fetching players:', error);
    }
  };

  return <PlayerContext.Provider value={{ players, fetchPlayers, isModalAdd, setModalAdd }}>{children}</PlayerContext.Provider>;
};

export const usePlayerContext = () => {
  const context = useContext(PlayerContext);
  if (!context) {
    throw new Error('usePlayerContext must be used within a PlayerProvider');
  }
  return context;
};

export default PlayerContext;
