import { useEffect, useState } from 'react';
import { TUser } from '../mocks/db/users.db';
import { userClient } from '../services/users';
import { authClient } from '../services/auth';

const useUserAPI = (username: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [response, setResponse] = useState<TUser | undefined>();

  const getCheckToken = async (token: string) => {
    try {
      const authResponse = await authClient.getCheckToken(token);
      setResponse(authResponse.data);
      return authResponse;
    } catch (error) {
      // Handle errors
      console.error('Error con endpoint:', error);
    }
  };

  const getCheckUser = async (userNameOrMail: string, password: string) => {
    try {
      const authResponse = await authClient.login({
        user: userNameOrMail,
        password,
      });

      // console.log('authResponse:', authResponse);
      setResponse(authResponse.data);

      // You may want to handle the response accordingly
      return authResponse;
    } catch (error: any) {
      // Handle errors
      // console.error('Error con endpoint:', error);

      if (error.response && error.response.status === 404) {
        // Unauthorized - incorrect username or password
        throw new Error(error.response.data.error);
      } else {
        // Other errors
        throw error;
      }
    }
  };

  const getcheckGoogle = async (token: string) => {
    try {
      const authResponse = await authClient.signInWithGoogle(token);

      console.log('authResponse signInWithGoogle:', authResponse);
      setResponse(authResponse.data);

      return authResponse;
    } catch (error: any) {
      // Handle errors
      // console.error('Error con endpoint:', error);
      if (error.response && error.response.status === 404) {
        // Unauthorized - incorrect username or password
        throw new Error(error.response.data.error);
      } else {
        // Other errors
        throw error;
      }
    }
  };

  const renewAccessToken = async (refreshToken: string) => {
    try {
      const authResponse = await authClient.renewAccessToken(refreshToken);
      setResponse(authResponse.data);
      return authResponse;
    } catch (error) {
      // Handle errors
      console.error('Error con endpoint:', error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        // const meResponse = await userClient.me();
        // setResponse(meResponse);
        setIsLoading(false);
      } catch (error) {
        // Handle errors
        console.error('Error fetching user data:', error);
        setIsLoading(false);
      }
    };

    fetchData();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [username]);

  return { response, isLoading, getCheckUser, getcheckGoogle, getCheckToken, renewAccessToken };
};

export default useUserAPI;
