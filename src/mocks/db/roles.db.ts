import { modulesDbList } from './modules.db';
import { TPermission } from '../../constants/permissions.constant';
import { guid } from '@fullcalendar/core/internal';

export type TRole = {
  id: string;
  guid: string;
  name: string;
  modules: { [key: string]: TPermission['value'] };
};

export const rolesDbList = {
  admin: {
    id: 'admin',
    guid: '1881e61d-4be8-4e3b-9482-9643d5c05326',
    name: 'Super Administrador',
    modules: {
      [modulesDbList.customer.guid]: 7 as TPermission['value'],
      [modulesDbList.product.guid]: 7 as TPermission['value'],
      [modulesDbList.category.guid]: 7 as TPermission['value'],
    },
  },
  user: {
    id: 'user',
    guid: '1881e61d-4be8-4e3b-9482-9643d5c05327',
    name: 'Administrador de liga',
    modules: {
      [modulesDbList.customer.guid]: 7 as TPermission['value'],
      [modulesDbList.product.guid]: 7 as TPermission['value'],
      [modulesDbList.category.guid]: 7 as TPermission['value'],
    },
  },
  customer: {
    id: 'customer',
    guid: '1881e61d-4be8-4e3b-9482-9643d5c05328',
    name: 'Entrenador/Dueño del equipo',
    modules: {
      [modulesDbList.customer.guid]: 0 as TPermission['value'],
      [modulesDbList.product.guid]: 4 as TPermission['value'],
      [modulesDbList.category.guid]: 4 as TPermission['value'],
    },
  },
  management: {
    id: 'management',
    guid: '1881e61d-4be8-4e3b-9482-9643d5c05329',
    name: 'Arbitro',
    modules: {
      [modulesDbList.customer.guid]: 7 as TPermission['value'],
      [modulesDbList.product.guid]: 7 as TPermission['value'],
      [modulesDbList.category.guid]: 7 as TPermission['value'],
    },
  },
};

const rolesDb: TRole[] = Object.values(rolesDbList);

export default rolesDb;
