import { TColors } from '../../types/colors.type';

type TCategoryKEY = 'technology' | 'shoes' | 'stationery' | 'glassware' | 'fashion' | 'watch' | string;
export type TCategory = {
  id: string;
  name: string;
  color: TColors;
  status: boolean;
  image?: string;
  imageSrc?: string;
};

export type TTypeCompetitions = {
  id: string;
  guid: string;
  name: string;
  imageSrc?: string;
  icon?: string;
  status: boolean;
};

export const TypesCompetitions: { [key in TTypeCompetitionsKEY]: TTypeCompetitions } = {
  Liga: {
    guid: 'f4d2b3e4-2f4e-4f1e-8c0d-7e8c5f6b9b1d',
    name: 'Liga',
    imageSrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2tN-mJclA5jsceB8h1Y_ZSs3lIDqGzwOFvA&s',
    icon: 'MdFormatListNumbered',
    status: true,
  },
  Copa: {
    guid: 'f4d2b3e4-2f4e-4f1e-8c0d-7e8c5f6b9b1e',
    name: 'Copa',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/1645/1645768.png',
    icon: 'BsSortNumericUpAlt',
    status: true,
  },
  Torneo: {
    guid: 'f4d2b3e4-2f4e-4f1e-8c0d-7e8c5f6b9b1f',
    name: 'Torneo',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/2761/2761875.png',
    icon: 'TbTournament',
    status: true,
  },
  Eliminatoria: {
    id: 'Eliminatoria',
    name: 'Eliminacion Directa',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/694/694606.png',
    icon: 'TfiLayoutGrid2',
    status: true,
  },
};

export const categoriesDbList: { [key in TCategoryKEY]: TCategory } = {
  technology: {
    id: 'technology',
    name: 'Futbol',
    imageSrc: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2tN-mJclA5jsceB8h1Y_ZSs3lIDqGzwOFvA&s',
    color: 'amber',
    status: true,
  },
  shoes: {
    id: 'shoes',
    name: 'Baloncesto',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/1645/1645768.png',
    color: 'sky',
    status: true,
  },
  stationery: {
    id: 'stationery',
    name: 'Voleibol',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/2761/2761875.png',
    color: 'lime',
    status: true,
  },
  glassware: {
    id: 'glassware',
    name: 'Beisbol',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/694/694606.png',
    color: 'sky',
    status: true,
  },
  fashion: {
    id: 'fashion',
    name: 'Softbol',
    imageSrc: 'https://png.pngtree.com/png-clipart/20200709/original/pngtree-slow-softball-png-image_2629022.jpg',
    color: 'emerald',
    status: true,
  },
  watch: {
    id: 'watch',
    name: 'Futbol Sala',
    imageSrc: 'https://cdn3d.iconscout.com/3d/premium/thumb/futsal-9543185-7812813.png',
    color: 'emerald',
    status: true,
  },
  tenis: {
    id: 'watch',
    name: 'Tenis',
    imageSrc: 'https://cdn-icons-png.flaticon.com/512/2633/2633933.png',
    color: 'emerald',
    status: true,
  },
};

const categoriesDb: TCategory[] = Object.values(categoriesDbList);
export default categoriesDb;
