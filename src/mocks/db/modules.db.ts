export type TModule = {
  guid: string;
  name: string;
};

export const modulesDbList = {
  customer: { guid: 'customer', name: 'Customer' },
  product: { guid: 'product', name: 'Product' },
  category: { guid: 'category', name: 'Category' },
};

const modulesDb: TModule[] = Object.values(modulesDbList);

export default modulesDb;
