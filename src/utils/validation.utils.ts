import { useUserContext } from '../context/userContext';

// Función para validar la unicidad del correo electrónico
export const useValidation = () => {
  const { users, fetchUsers } = useUserContext();

  const validateEmailUniquenes = async (value, originalValue) => {
    try {
      // Asegúrate de que `users` esté cargado correctamente
      if (!users || users.length === 0) {
        await fetchUsers(); // Si los usuarios no están cargados, cárgalos
      }

      // Encuentra el usuario con el correo electrónico proporcionado
      const user = users.find((u) => u.email === value);

      // Verifica si el correo electrónico ha cambiado o si es único
      if (originalValue === value) {
        return true; // El correo no ha cambiado
      }
      return !(user && user.email === value); // Devuelve false si el correo ya existe
    } catch (error) {
      console.error('Error al verificar el correo electrónico:', error);
      throw new Error('Error al verificar el correo electrónico'); // Lanza un error para manejarlo en Yup
    }
  };

  // Función para validar la unicidad del número de teléfono
  const validatePhoneUniquenes = async (value, originalValue) => {
    try {
      // Asegúrate de que `users` esté cargado correctamente
      if (!users || users.length === 0) {
        await fetchUsers(); // Si los usuarios no están cargados, cárgalos
      }

      // Encuentra el usuario con el número de teléfono proporcionado
      const user = users.find((u) => u.phone === value);

      if (originalValue === value) {
        return true; // El número de teléfono no ha cambiado
      }
      return !(user && user.phone === value); // Devuelve false si el teléfono ya existe
    } catch (error) {
      console.error('Error al verificar el teléfono:', error);
      throw new Error('Error al verificar el teléfono'); // Lanza un error para manejarlo en Yup
    }
  };

  // Función para validar la unicidad del username
  const validateUsernameUniquenes = async (value, originalValue) => {
    try {
      // Asegúrate de que `users` esté cargado correctamente
      if (!users || users.length === 0) {
        await fetchUsers(); // Si los usuarios no están cargados, cárgalos
      }

      // Encuentra el usuario con el username proporcionado
      const user = users.find((u) => u.username === value);

      if (originalValue === value) {
        return true; // El username no ha cambiado
      }
      return !(user && user.username === value); // Devuelve false si el username ya existe
    } catch (error) {
      console.error('Error al verificar el username:', error);
      throw new Error('Error al verificar el username'); // Lanza un error para manejarlo en Yup
    }
  };

  return { validateEmailUniquenes, validatePhoneUniquenes, validateUsernameUniquenes };
};
