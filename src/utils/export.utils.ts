// export.utils.ts

// exportUtils.js

import { saveAs } from 'file-saver';
import * as XLSX from 'xlsx';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';

export const exportCSV = (data: any[], columns: any[], filename = 'data.csv') => {
  // Lógica de exportación CSV
  const exportableColumns = columns.filter((col) => col.exportable);
  const headers = exportableColumns.map((col) => col.header);
  const csvContent = `data:text/csv;charset=utf-8,${headers.join(',')}\r\n`;
  const rows = data.map((row) => {
    return exportableColumns.map((col) => {
      let cellValue = '';
      if (col) {
        const accessorKeys = col.accessorKey.split('.');
        let tempObj = row;
        for (const key of accessorKeys) {
          tempObj = tempObj[key];
        }
        cellValue = tempObj;
      }
      return cellValue;
    });
  });
  const csvRows = rows.map((row) => row.join(',')).join('\r\n');
  const csvData = csvContent + csvRows;
  const blob = new Blob([csvData], { type: 'text/csv;charset=utf-8;' });
  saveAs(blob, filename);
};

export const exportExcel = (data: any[], columns: any[], filename = 'data.xlsx') => {
  // Lógica de exportación Excel
  const exportableColumns = columns.filter((col) => col.exportable);
  const headers = exportableColumns.map((col) => col.header);
  const rows = data.map((row) => {
    const rowData: any[] = [];
    exportableColumns.forEach((col) => {
      let cellValue = '';
      if (col) {
        const accessorKeys = col.accessorKey.split('.');
        let tempObj = row;
        for (const key of accessorKeys) {
          tempObj = tempObj[key];
        }
        cellValue = tempObj;
      }
      rowData.push(cellValue);
    });
    return rowData;
  });
  const worksheet = XLSX.utils.aoa_to_sheet([headers, ...rows]);
  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet 1');
  XLSX.writeFile(workbook, filename);
};

export const exportPDF = (data: any[], columns: any[], filename = 'data.pdf') => {
  // Lógica de exportación PDF
  const doc = new jsPDF();

  // Filtrar columnas exportables
  const exportableColumns = columns.filter((col) => col.exportable);

  // Mapear las cabeceras de las columnas exportables
  const tableColumn = exportableColumns.map((col) => col.header);

  // Preparar las filas de la tabla
  const tableRows = data.map((row) => {
    return exportableColumns.map((col) => {
      // Inicializa el valor de la celda como una cadena vacía
      let cellValue = '';

      // Verifica si el accessor de la columna está definido y es una cadena
      if (col) {
        // Si es un accessor anidado
        const accessorKeys = col.accessorKey.split('.');
        let tempObj = row;

        for (const key of accessorKeys) {
          tempObj = tempObj[key];
        }

        cellValue = tempObj;
      }

      return cellValue;
    });
  });

  // Agregar la tabla al documento PDF
  doc.autoTable({
    head: [tableColumn],
    body: tableRows,
  });

  // Guardar el archivo PDF
  doc.save(filename);
};
