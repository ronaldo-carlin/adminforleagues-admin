import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { GoogleOAuthProvider } from '@react-oauth/google';
import reportWebVitals from './reportWebVitals';

import { ThemeContextProvider } from './context/themeContext';
import { AuthProvider } from './context/authContext';
import App from './App/App';

import './i18n';
import './styles/index.css';

import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css';
import './styles/vendors.css';
import 'react-toastify/dist/ReactToastify.css';
import { AxiosProvider } from './services/AxiosProvider';
import { UserProvider } from './context/userContext';
import { ToastProvider } from './context/ToastContext';
import { AccessControlProvider } from './context/accessControlContext';
import { TeamProvider } from './context/teamContext';
import { PlayerProvider } from './context/playerContext';
import { CompetitionProvider } from './context/competitionContext';
import { SedeProvider } from './context/SedeContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <GoogleOAuthProvider clientId={import.meta.env.VITE_GOOGLE_CLIENT_ID}>
    <ThemeContextProvider>
      <BrowserRouter>
        <AuthProvider>
          <AxiosProvider>
            <AccessControlProvider>
              <UserProvider>
                <TeamProvider>
                  <PlayerProvider>
                    <CompetitionProvider>
                      <SedeProvider>
                        <ToastProvider>
                          <App />
                        </ToastProvider>
                      </SedeProvider>
                    </CompetitionProvider>
                  </PlayerProvider>
                </TeamProvider>
              </UserProvider>
            </AccessControlProvider>
          </AxiosProvider>
        </AuthProvider>
      </BrowserRouter>
    </ThemeContextProvider>
  </GoogleOAuthProvider>,
);

reportWebVitals();
