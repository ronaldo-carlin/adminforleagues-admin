import { TAccessControl, TCreateAccessControl, TDeteleAccessControl, TUpdateAccessControl, TAccessControlByRole } from '../types/access_control';
import { TypeApiResponse } from '../types/api';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const Access_Control = 'tenant_access_control/';

export const AccessControlClient = {
  Add: (data: TCreateAccessControl) => {
    return HttpClient.post<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROL_CREATE}`, data);
  },
  Update: ({ guid, ...data }: TUpdateAccessControl) => {
    return HttpClient.patch<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROL_UPDATE}/${guid}`, data);
  },
  AccessControls: () => {
    return HttpClient.get<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROLS}`);
  },
  AccessControl: ({ guid }: TAccessControl) => {
    return HttpClient.get<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROL}/${guid}`);
  },
  Delete: ({ guid }: TDeteleAccessControl) => {
    return HttpClient.delete<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROL_DELETE}/${guid}`);
  },
  // petición para obtener los accesos de un rol
  AccessControlByRol: ({ guid }: TAccessControlByRole) => {
    return HttpClient.get<TypeApiResponse>(`${Access_Control}${API_ENDPOINTS.ACCESS_CONTROL_BY_ROL}/${guid}`);
  },
};
