import { UpdateCategories, TCategories } from '../types/categories';
import { AuthResponse, RegisterInput } from '../types/users';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const CATEGORIES = 'tenant_categories/';

export const categoryClient = {
  AddCategory: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(CATEGORIES + API_ENDPOINTS.CATEGORY_CREATE, variables);
  },
  UpdateCategory: ({ guid, ...inputs }: UpdateCategories) => {
    return HttpClient.patch<TCategories>(`${CATEGORIES}${API_ENDPOINTS.CATEGORY_UPDATE}/${guid}`, inputs);
  },
  Categories_Sport: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${CATEGORIES}${API_ENDPOINTS.CATEGORIES}/${guid}`);
  },
  Categories: () => {
    return HttpClient.get<any>(CATEGORIES + API_ENDPOINTS.CATEGORIES);
  },
  Category: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${CATEGORIES}${API_ENDPOINTS.CATEGORIES}/${guid}`);
  },
  DeleteCategory: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${CATEGORIES}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
