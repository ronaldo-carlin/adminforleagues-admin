import {
  AuthResponse,
  LoginInput,
  RegisterInput,
  ChangePasswordInput,
  ForgetPasswordInput,
  VerifyForgetPasswordTokenInput,
  ResetPasswordInput,
} from '../types';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const AUTH = 'tenant_auth/';

export const authClient = {
  login: (variables: LoginInput) => {
    return HttpClient.post<AuthResponse>(AUTH + API_ENDPOINTS.LOGIN, variables);
  },
  logout: () => {
    return HttpClient.post<any>(API_ENDPOINTS.LOGOUT, {});
  },
  register: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(API_ENDPOINTS.REGISTER, variables);
  },
  changePassword: (variables: ChangePasswordInput) => {
    return HttpClient.post<any>(API_ENDPOINTS.CHANGE_PASSWORD, variables);
  },
  forgetPassword: (variables: ForgetPasswordInput) => {
    return HttpClient.post<any>(API_ENDPOINTS.FORGET_PASSWORD, variables);
  },
  verifyForgetPasswordToken: (variables: VerifyForgetPasswordTokenInput) => {
    return HttpClient.post<any>(API_ENDPOINTS.VERIFY_FORGET_PASSWORD_TOKEN, variables);
  },
  resetPassword: (variables: ResetPasswordInput) => {
    return HttpClient.post<any>(API_ENDPOINTS.RESET_PASSWORD, variables);
  },
  signInWithGoogle: (token: string) => {
    return HttpClient.post<AuthResponse>(AUTH + API_ENDPOINTS.GOOGLE_LOGIN, { token });
  },
  getCheckToken: (token: string) => {
    return HttpClient.post<AuthResponse>(AUTH + API_ENDPOINTS.CHECKTOKEN, { token });
  },
  renewAccessToken: (refreshToken: string) => {
    return HttpClient.post<AuthResponse>(AUTH + API_ENDPOINTS.RENEW_ACCESS_TOKEN, {
      refreshToken,
    });
  },
};
