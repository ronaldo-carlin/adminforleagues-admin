import { TModules } from '../types/modules';
import { TypeApiResponse } from '../types/api';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const Modules = 'tenant_modules/';

export const ModulesClient = {
  Add: (data: TModules) => {
    return HttpClient.post<TypeApiResponse>(`${Modules}${API_ENDPOINTS.MODULE_CREATE}`, data);
  },
  Update: ({ guid, ...data }: TModules) => {
    return HttpClient.patch<TypeApiResponse>(`${Modules}${API_ENDPOINTS.MODULE_UPDATE}/${guid}`, data);
  },
  Modules: () => {
    return HttpClient.get<TypeApiResponse>(`${Modules}${API_ENDPOINTS.MODULES}`);
  },
  Module: ({ guid }: TModules) => {
    return HttpClient.get<TypeApiResponse>(`${Modules}${API_ENDPOINTS.MODULE}/${guid}`);
  },
  Delete: ({ guid }: TModules) => {
    return HttpClient.delete<TypeApiResponse>(`${Modules}${API_ENDPOINTS.MODULE_DELETE}/${guid}`);
  },
};
