import { UpdateSport, TSport } from '../types/sports';
import { AuthResponse, RegisterInput } from '../types/users';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const SPORT = 'tenant_sports/';

export const sportClient = {
  AddSport: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(SPORT + API_ENDPOINTS.SPORT_CREATE, variables);
  },
  UpdateSport: ({ guid, ...inputs }: UpdateSport) => {
    return HttpClient.patch<TSport>(`${SPORT}${API_ENDPOINTS.SPORT_UPDATE}/${guid}`, inputs);
  },
  Sports: () => {
    return HttpClient.get<any>(SPORT + API_ENDPOINTS.SPORTS);
  },
  Sport: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${SPORT}${API_ENDPOINTS.SPORT}/${guid}`);
  },
  DeleteSport: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${SPORT}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
