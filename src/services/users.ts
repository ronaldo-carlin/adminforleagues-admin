import { AuthResponse, RegisterInput, UpdateUser, TUser } from '../types/users';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const USER = 'tenant_users/';

export const userClient = {
  Add: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(USER + API_ENDPOINTS.USER_CREATE, variables);
  },
  update: ({ guid, ...inputs }: UpdateUser) => {
    return HttpClient.patch<TUser>(`${USER}${API_ENDPOINTS.USER_UPDATE}/${guid}`, inputs);
  },
  users: () => {
    return HttpClient.get<any>(USER + API_ENDPOINTS.USERS);
  },
  user: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${USER}${API_ENDPOINTS.USER}/${guid}`);
  },
  delete: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${USER}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
