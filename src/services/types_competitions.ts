import { UpdateTypesCompetitions, TTypes_competitions } from '../types/types_competitions';
import { AuthResponse, RegisterInput } from '../types/users';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const TYPES_COMPETITIONS = 'tenant_types_competitions/';

export const typeCompetitionClient = {
  AddTypesCompetition: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(TYPES_COMPETITIONS + API_ENDPOINTS.TYPES_COMPETITIONS_CREATE, variables);
  },
  UpdateTypesCompetition: ({ guid, ...inputs }: UpdateTypesCompetitions) => {
    return HttpClient.patch<TTypes_competitions>(`${TYPES_COMPETITIONS}${API_ENDPOINTS.TYPES_COMPETITIONS_UPDATE}/${guid}`, inputs);
  },
  TypesCompetitions: () => {
    return HttpClient.get<any>(TYPES_COMPETITIONS + API_ENDPOINTS.TYPES_COMPETITIONS);
  },
  TypesCompetition: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${TYPES_COMPETITIONS}${API_ENDPOINTS.TYPES_COMPETITIONS}/${guid}`);
  },
  DeleteTypesCompetition: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${TYPES_COMPETITIONS}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
