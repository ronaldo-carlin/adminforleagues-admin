import { UpdateSede, TSede } from '../types/sedes';
import { AuthResponse, RegisterInput } from '../types/users';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const SEDE = 'tenant_sedes/';

export const sedeClient = {
  AddSede: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(SEDE + API_ENDPOINTS.SEDE_CREATE, variables);
  },
  UpdateSede: ({ guid, ...inputs }: UpdateSede) => {
    return HttpClient.patch<TSede>(`${SEDE}${API_ENDPOINTS.SEDE_UPDATE}/${guid}`, inputs);
  },
  Sedes: () => {
    return HttpClient.get<any>(SEDE + API_ENDPOINTS.SEDES);
  },
  Sede: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${SEDE}${API_ENDPOINTS.SEDE}/${guid}`);
  },
  DeleteSede: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${SEDE}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
