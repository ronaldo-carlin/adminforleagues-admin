import { AuthResponse, RegisterInput, UpdateCompetition, TCompetition } from '../types/competitions';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const COMPETITION = 'tenant_competitions/';

export const competitionClient = {
  AddCompetition: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(COMPETITION + API_ENDPOINTS.COMPETITION_CREATE, variables);
  },
  UpdateCompetition: ({ guid, ...inputs }: UpdateCompetition) => {
    return HttpClient.patch<TCompetition>(`${COMPETITION}${API_ENDPOINTS.COMPETITION_UPDATE}/${guid}`, inputs);
  },
  Competitions: () => {
    return HttpClient.get<any>(COMPETITION + API_ENDPOINTS.COMPETITIONS);
  },
  Competition: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${COMPETITION}${API_ENDPOINTS.COMPETITION}/${guid}`);
  },
  DeleteCompetition: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${COMPETITION}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
