import { AuthResponse, RegisterInput, UpdatePlayer, TPlayer } from '../types/players';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const PLAYER = 'tenant_players/';

export const playerClient = {
  AddPlayer: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(PLAYER + API_ENDPOINTS.PLAYER_CREATE, variables);
  },
  UpdatePlayer: ({ guid, ...inputs }: UpdatePlayer) => {
    return HttpClient.patch<TPlayer>(`${PLAYER}${API_ENDPOINTS.PLAYER_UPDATE}/${guid}`, inputs);
  },
  Players: () => {
    return HttpClient.get<any>(PLAYER + API_ENDPOINTS.PLAYERS);
  },
  Player: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${PLAYER}${API_ENDPOINTS.PLAYER}/${guid}`);
  },
  DeletePlayer: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${PLAYER}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
