import { TPermissions } from '../types/permissions';
import { TypeApiResponse } from '../types/api';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const Permissions = 'tenant_permissions/';

export const PermissionsClient = {
  Add: (data: TPermissions) => {
    return HttpClient.post<TypeApiResponse>(`${Permissions}${API_ENDPOINTS.PERMISSION_CREATE}`, data);
  },
  Update: ({ guid, ...data }: TPermissions) => {
    return HttpClient.patch<TypeApiResponse>(`${Permissions}${API_ENDPOINTS.PERMISSION_UPDATE}/${guid}`, data);
  },
  Permissions: () => {
    return HttpClient.get<TypeApiResponse>(`${Permissions}${API_ENDPOINTS.PERMISSIONS}`);
  },
  Permission: ({ guid }: TPermissions) => {
    return HttpClient.get<TypeApiResponse>(`${Permissions}${API_ENDPOINTS.PERMISSION}/${guid}`);
  },
  Delete: ({ guid }: TPermissions) => {
    return HttpClient.delete<TypeApiResponse>(`${Permissions}${API_ENDPOINTS.PERMISSION_DELETE}/${guid}`);
  },
};
