import { AuthResponse, RegisterInput, UpdateTeam, TTeam } from '../types/teams';
import { API_ENDPOINTS } from './api-endpoints';
import { HttpClient } from './AxiosProvider';

const TEAM = 'tenant_teams/';

export const teamClient = {
  AddTeam: (variables: RegisterInput) => {
    return HttpClient.post<AuthResponse>(TEAM + API_ENDPOINTS.TEAM_CREATE, variables);
  },
  UpdateTeam: ({ guid, ...inputs }: UpdateTeam) => {
    return HttpClient.patch<TTeam>(`${TEAM}${API_ENDPOINTS.TEAM_UPDATE}/${guid}`, inputs);
  },
  Teams: () => {
    return HttpClient.get<any>(TEAM + API_ENDPOINTS.TEAMS);
  },
  Team: ({ guid }: { guid: string }) => {
    return HttpClient.get<any>(`${TEAM}${API_ENDPOINTS.TEAM}/${guid}`);
  },
  DeleteTeam: ({ guid }: { guid: string }) => {
    return HttpClient.delete<any>(`${TEAM}${API_ENDPOINTS.USER_DELETE}/${guid}`);
  },
};
