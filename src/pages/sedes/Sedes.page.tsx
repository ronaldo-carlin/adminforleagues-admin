import React, { useContext, useEffect, useState } from 'react';
import { table } from 'console';
import PropagateLoader from 'react-spinners/PropagateLoader';
import { columns } from 'tailwindcss/defaultTheme';
import TableTemplate, { TableCardFooterTemplate } from '../../templates/common/TableParts.template';
// Componentes
import PageWrapper from '../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../components/layouts/Container/Container';
import Badge from '../../components/ui/Badge';
import Button from '../../components/ui/Button';
import Card, { CardHeader, CardHeaderChild, CardTitle, CardBody } from '../../components/ui/Card';
import Dropdown, { DropdownToggle, DropdownMenu, DropdownItem } from '../../components/ui/Dropdown';
import Avatar from '../../components/Avatar';
import Icon from '../../components/icon/Icon';
import Tooltip from '../../components/ui/Tooltip';
// utils
import { exportPDF, exportExcel, exportCSV } from '../../utils/export.utils';
// context
import SedeContext from '../../context/SedeContext';
import { useAuth } from '../../context/authContext';
// Services
import { sedeClient } from '../../services/sedes';
import { ToastContainer } from 'react-toastify';
import {
  useReactTable,
  getCoreRowModel,
  getFilteredRowModel,
  getSortedRowModel,
  getPaginationRowModel,
  FilterFnOption,
  ColumnFiltersState,
  SortingState,
  createColumnHelper,
} from '@tanstack/react-table';
// Types
import { Link } from 'react-router-dom';
import { TSede } from '../../types/sedes';
import { adminPages } from '../../config/pages.config';
import FormAddSede from '../../components/form/AddSede';
import Modal, { ModalHeader, ModalBody, ModalFooter, ModalFooterChild } from '../../components/ui/Modal';
import FieldWrap from '../../components/form/FieldWrap';
import Input from '../../components/form/Input';
import Subheader, { SubheaderLeft, SubheaderRight } from '../../components/layouts/Subheader/Subheader';

const columnHelper = createColumnHelper<TSede>();
const editLinkPath = `../${adminPages.editUsersPage.to}/`;

const Sedes = () => {
  const sedeContext = useContext(SedeContext);

  const { getToken } = useAuth();
  // estado para el ordenamiento de la tabla
  const [sorting, setSorting] = useState<SortingState>([]);
  // estado para el filtro global
  const [globalFilter, setGlobalFilter] = useState<string>('');
  // estado para saber si se está cargando la información
  const [isLoading, setIsLoading] = useState<boolean>(false);
  // estado para abrir y cerrar el modal de eliminar estadio
  const [isModalOpen, setModal] = useState<boolean>(false);
  // estado para guardar el guid del estadio a eliminar
  const [guidDelete, setGuidDelete] = useState<string>('');

  const [columnFilters, setColumnFilters] = useState<ColumnFiltersState>([]); // Puede establecer aquí el estado inicial del filtro de columna

  const columns = [
    columnHelper.accessor('image_path', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <Avatar
            src={info.row.original.image_path}
            name={`${info.row.original.name}`}
            className="!aspect-[9/12] !w-12 2xl:!w-20"
            rounded="rounded"
          />
        </Link>
      ),
      header: 'Imagen',
      footer: 'Imagen',
      enableGlobalFilter: false,
      enableColumnFilter: false,
      enableSorting: false,
      exportable: false,
    }),
    columnHelper.accessor('name', {
      cell: (info: { row: { original: TSede } }) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold">{`${info.row.original.name}`}</div>
        </Link>
      ),
      header: 'Nombre',
      footer: 'Nombre',
      enableGlobalFilter: true,
      enableColumnFilter: true,
      exportable: true,
    }),
    columnHelper.accessor('schedule', {
      cell: (info: { row: { original: TSede } }) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold">{`${info.row.original.schedule}`}</div>
        </Link>
      ),
      header: 'Horario',
      footer: 'Horario',
      enableGlobalFilter: true,
      enableColumnFilter: true,
      exportable: true,
    }),
    columnHelper.accessor('address', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold">{`${info.row.original.address}`}</div>
        </Link>
      ),
      header: 'Dirección',
      footer: 'Dirección',
      enableGlobalFilter: true,
      enableColumnFilter: true,
      enableResizing: true,
      size: 500,
      exportable: true,
    }),
    columnHelper.accessor('state', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold text-2xl">{`${info.row.original.state}`}</div>
        </Link>
      ),
      header: 'Estado',
      footer: 'Estado',
      enableGlobalFilter: false,
      enableColumnFilter: false,
      enableSorting: false,
      exportable: true,
    }),
    columnHelper.accessor('actions', {
      cell: (info) => (
        <div className="flex items-center gap-2">
          <Link to={`${editLinkPath}${info.row.original.guid}`}>
            <Tooltip text="Editar">
              <Button variant="outline" icon="HeroPencil" color="amber" />
            </Tooltip>
          </Link>
          <Tooltip text="Eliminar">
            <Button
              onClick={() => {
                setModal(true);
                setGuidDelete(info.row.original.guid);
              }}
              color="red"
              icon="HeroTrash"
              variant="outline"
            />
          </Tooltip>
        </div>
      ),
      header: 'Acciones',
      footer: 'Acciones',
      enableColumnFilter: false,
      enableGlobalFilter: false,
      enableSorting: false,
      exportable: false,
    }),
  ];

  const table = useReactTable({
    data: sedeContext?.sedes || [],
    columns,
    state: {
      sorting,
      globalFilter,
      columnFilters,
    },
    onColumnFiltersChange: setColumnFilters,
    onSortingChange: setSorting,
    enableGlobalFilter: true,
    onGlobalFilterChange: setGlobalFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: { pageSize: 5 },
    },
    globalFilterFn: 'fuzzy' as FilterFnOption<TSede>, // or choose the desired filter function type
    defaultColumn: {
      size: 300,
      minSize: 200,
    },
  });

  // funcion para eliminar estadio
  const handleDeleteSede = async (guid: string) => {
    console.log('Estadio eliminado:', guid);
    try {
      const response = await sedeClient.DeleteSede({ guid });
      console.log('Estadio eliminado:', response);
      sedeContext?.fetchSedes();
    } catch (error) {
      console.error('Error eliminando estadio:', error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true); // Marca como cargando
      if (sedeContext?.sedes.length) {
        setIsLoading(false); // Marca como no cargando
        return;
      } // Evita solicitudes duplicadas
      const token = await getToken(); // Espera a que el token esté disponible
      if (token) {
        await sedeContext.fetchSedes(); // Llama a fetchSedes si el token está disponible
      }
      setIsLoading(false); // Marca como no cargando
    };
    fetchData(); // Llama a la función fetchData
  }, [getToken]); // Incluye isLoading como dependencia

  return (
    <PageWrapper name="Estadios">
      {/* Modal para eliminar estadios */}
      <Modal isOpen={isModalOpen} setIsOpen={setModal}>
        <ModalHeader>Eliminar Estadio</ModalHeader>
        <ModalBody>
          ¿Estás seguro de eliminar a{' '}
          {`${sedeContext?.sedes.find((sede) => sede.guid === guidDelete)?.name} ${sedeContext?.sedes.find((sede) => sede.guid === guidDelete)
            ?.name}`}
          ?
        </ModalBody>
        <ModalFooter>
          <ModalFooterChild>
            <Button onClick={() => setModal(false)} variant="outline">
              Cancelar
            </Button>
          </ModalFooterChild>
          <ModalFooterChild>
            <Button
              onClick={() => {
                setModal(false);
                handleDeleteSede(guidDelete);
              }}
              variant="solid"
            >
              Eliminar
            </Button>
          </ModalFooterChild>
        </ModalFooter>
      </Modal>

      {/* Modal para agregar nuevos estadios */}
      <Modal isOpen={sedeContext?.isModalAdd} setIsOpen={sedeContext?.setModalAdd}>
        <ModalHeader>Agregar Estadio</ModalHeader>
        <ModalBody>
          <FormAddSede />
        </ModalBody>
      </Modal>

      <Subheader>
        <SubheaderLeft>
          <FieldWrap
            firstSuffix={<Icon className="mx-2" icon="HeroMagnifyingGlass" />}
            lastSuffix={globalFilter && <Icon icon="HeroXMark" color="red" className="mx-2 cursor-pointer" onClick={() => setGlobalFilter('')} />}
          >
            <Input id="search" name="search" placeholder="Buscar..." value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} />
          </FieldWrap>
        </SubheaderLeft>
        <SubheaderRight>
          <Button
            variant="solid"
            icon="HeroPlus"
            onClick={() => {
              sedeContext?.setModalAdd(true);
            }}
          >
            Agregar Estadio
          </Button>
        </SubheaderRight>
      </Subheader>

      <Container className="flex h-full items-center justify-center">
        <Card className="h-full w-full">
          <CardHeader>
            <CardHeaderChild>
              <CardTitle>Todos los Estadios</CardTitle>
              <Badge variant="outline" className="border-transparent px-4" rounded="rounded-full">
                {table.getFilteredRowModel().rows.length} Registros
              </Badge>
            </CardHeaderChild>
            <CardHeaderChild>
              <Dropdown>
                <DropdownToggle>
                  <Button icon="HeroRocketLaunch">Exportar</Button>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => exportCSV(sedeContext?.sedes, columns, 'sedes.csv')} icon="HeroDocumentDownload">
                    Exportar CSV
                  </DropdownItem>
                  <DropdownItem onClick={() => exportExcel(sedeContext?.sedes, columns, 'sedes.xlsx')} icon="HeroDocumentDownload">
                    Exportar Excel
                  </DropdownItem>
                  <DropdownItem onClick={() => exportPDF(sedeContext?.sedes, columns, 'sedes.pdf')} icon="HeroDocumentDownload">
                    Exportar PDF
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </CardHeaderChild>
          </CardHeader>

          <CardBody className="overflow-auto">
            {isLoading ? (
              <div className="flex h-96 items-center justify-center">
                <PropagateLoader size={15} color="#ffcc00" loading={isLoading} speedMultiplier={0.8} />
              </div>
            ) : (
              <TableTemplate className="table-fixed max-md:min-w-[100rem]" table={table} />
            )}
          </CardBody>
          <TableCardFooterTemplate table={table} />
        </Card>
      </Container>

      {/* Mostrar el toast si el estado lo indica */}
      <ToastContainer stacked draggable />
    </PageWrapper>
  );
};

export default Sedes;
