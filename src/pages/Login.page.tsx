import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import classNames from 'classnames';
import { Link, Navigate } from 'react-router-dom';
import GoogleLogin, { GoogleLogout } from 'react-google-login';
import { useGoogleLogin } from '@react-oauth/google';
import { gapi } from 'gapi-script';
import PageWrapper from '../components/layouts/PageWrapper/PageWrapper';
import Button from '../components/ui/Button';
import { useAuth } from '../context/authContext';
import Input from '../components/form/Input';
import usersDb from '../mocks/db/users.db';
import LogoTemplate from '../templates/layouts/Logo/Logo.template';
import FieldWrap from '../components/form/FieldWrap';
import Icon from '../components/icon/Icon';
import Validation from '../components/form/Validation';
import { userClient } from '../services/users';

type TValues = {
  username: string;
  password: string;
};

const LoginPage = () => {
  const { onLogin, onLoginGoogle, isAuthenticated, getToken } = useAuth();

  const [passwordShowStatus, setPasswordShowStatus] = useState<boolean>(false);
  const [redirectToHome, setRedirectToHome] = useState<boolean>(false);

  const id_client = import.meta.env.VITE_GOOGLE_CLIENT_ID;

  // let accessToken = gapi.auth.getToken().access_token;

  const Login = useGoogleLogin({
    onSuccess: (tokenResponse) => {
      console.log('tokenResponse', tokenResponse);
    },
  });

  const handleGoogleLogin = async (res) => {
    try {
      function start() {
        gapi.client.init({
          clientId: id_client,
          scope: '',
        });
      }

      gapi.load('client:auth2', start);
      await onLoginGoogle();
    } catch (error) {
      console.error('Error en el login con Google:', error);
    }
  };

  const handleGoogleLoginFailure = (error) => {
    if (error.error === 'popup_closed_by_user') {
      // Manejar específicamente el cierre de la ventana por parte del usuario
      console.log('El usuario cerró la ventana de inicio de sesión.');
      // Aquí podrías invocar una función que maneje este caso específico
    } else {
      // Manejar otros errores de inicio de sesión
      console.error('Error en inicio de sesión con Google:', error);
    }
  };

  const formik = useFormik({
    initialValues: {
      username: 'admin@adminforleagues.com',
      password: 'Admin+.',
    },
    validate: (values: TValues) => {
      const errors: Partial<TValues> = {};

      if (!values.username) {
        errors.username = 'Se requiere el correo electrónico o el nombre de usuario';
      } else if (values.username.length > 255) {
        errors.username = 'El nombre de usuario debe tener menos de 255 caracteres';
      }

      if (!values.password) {
        errors.password = 'Se requiere contraseña';
      } else if (values.password.length > 255) {
        errors.password = 'La contraseña debe tener menos de 255 caracteres';
      }

      return errors;
    },
    onSubmit: async (values: TValues, { setErrors }) => {
      try {
        await onLogin(values.username, values.password);
      } catch (e) {
        if (e.message) {
          // Configura el mensaje de error en los campos correspondientes

          if (e.message === 'El usuario no existe.') {
            setErrors({
              ...formik.errors,
              username: 'Usuario no encontrado',
              password: '.', // Limpiar el error de contraseña solo si hay una contraseña
            });
          } else if (e.message === 'El usuario ha sido deshabilitado.') {
            setErrors({
              ...formik.errors,
              username: 'Usuario no activo',
              password: values.password ? formik.errors.password : '', // Limpiar el error de contraseña solo si hay una contraseña
            });
          } else if (e.response && e.response.data && e.response.data.message === 'Datos incorrectos.') {
            setErrors({
              ...formik.errors,
              password: 'Contraseña incorrecta',
              username: values.username ? formik.errors.username : '', // Limpiar el error de usuario solo si hay un nombre de usuario
            });
          } else {
            console.error('Error en el login:', e);
          }
        } else {
          // Otros casos de error
          console.error('Error en el login:', e);
        }
      }
    },
  });

  // valido si ya hay un usuario autenticado lo redirijo a la página principal
  useEffect(() => {
    if (isAuthenticated) {
      setRedirectToHome(true);
    }
  }, [isAuthenticated, getToken()]);

  if (redirectToHome) {
    return <Navigate to="/" />;
  }

  return (
    <PageWrapper isProtectedRoute={false} className="bg-white dark:bg-inherit" name="Sign In">
      <div className="container mx-auto flex h-full items-center justify-center">
        <div className="flex max-w-sm flex-col gap-8">
          <div>
            <LogoTemplate className="h-12" />
          </div>
          <div>
            <span className="text-4xl font-semibold">Sign in</span>
          </div>
          <div>
            <span>Regístrese con una cuenta abierta</span>
          </div>
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-6">
              <GoogleLogin
                clientId={id_client}
                buttonText="Google"
                onSuccess={handleGoogleLogin}
                onFailure={handleGoogleLoginFailure}
                cookiePolicy="single_host_origin"
                isSignedIn={false} // hace que inicie sesion en automatico
                className="w-full"
              />
            </div>
            <div className="col-span-6">
              <Button icon="CustomApple" variant="outline" color="zinc" size="lg" className="w-full">
                Apple
              </Button>
            </div>
          </div>
          <div className="border border-zinc-500/25 dark:border-zinc-500/50" />
          <div>
            <span>O continuar con la dirección de correo electrónico</span>
          </div>
          <form className="flex flex-col gap-4" noValidate>
            <div
              className={classNames({
                'mb-2': !formik.isValid,
              })}
            >
              <Validation isValid={formik.isValid} isTouched={formik.touched.username} invalidFeedback={formik.errors.username} validFeedback="Good">
                <FieldWrap firstSuffix={<Icon icon="HeroEnvelope" className="mx-2" />}>
                  <Input
                    dimension="lg"
                    id="username"
                    autoComplete="username"
                    name="username"
                    placeholder="Email or username"
                    value={formik.values.username}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </FieldWrap>
              </Validation>
            </div>
            <div
              className={classNames({
                'mb-2': !formik.isValid,
              })}
            >
              <Validation isValid={formik.isValid} isTouched={formik.touched.password} invalidFeedback={formik.errors.password} validFeedback="Good">
                <FieldWrap
                  firstSuffix={<Icon icon="HeroKey" className="mx-2" />}
                  lastSuffix={
                    <Icon
                      className="mx-2 cursor-pointer"
                      icon={passwordShowStatus ? 'HeroEyeSlash' : 'HeroEye'}
                      onClick={() => {
                        setPasswordShowStatus(!passwordShowStatus);
                      }}
                    />
                  }
                >
                  <Input
                    dimension="lg"
                    type={passwordShowStatus ? 'text' : 'password'}
                    autoComplete="current-password"
                    id="password"
                    name="password"
                    placeholder="Password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </FieldWrap>
              </Validation>
            </div>
            <div>
              <Button size="lg" variant="solid" className="w-full font-semibold" onClick={() => formik.handleSubmit()}>
                Iniciar sesión
              </Button>
            </div>
          </form>
          <div>
            <span className="text-zinc-500">Este sitio está protegido por reCAPTCHA y la Política de privacidad de Google.</span>
          </div>
          <div>
            <span className="flex gap-2 text-sm">
              <span className="text-zinc-400 dark:text-zinc-600">¿No tienes una cuenta?</span>
              <Link to="/" className="hover:text-inherit">
                Registrate
              </Link>
            </span>
          </div>
        </div>
      </div>
    </PageWrapper>
  );
};

export default LoginPage;
