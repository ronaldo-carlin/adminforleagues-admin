import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { PropagateLoader } from 'react-spinners';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import classNames from 'classnames';
// Componentes
import PageWrapper from '../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../components/layouts/Container/Container';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../components/ui/Card';
import Label from '../../components/form/Label';
import Input from '../../components/form/Input';
import RichText from '../../components/RichText';
import Subheader, { SubheaderLeft, SubheaderRight, SubheaderSeparator } from '../../components/layouts/Subheader/Subheader';
import Button from '../../components/ui/Button';
import Badge from '../../components/ui/Badge';
import Modal, { ModalBody, ModalHeader } from '../../components/ui/Modal';
import FormAddCategory from '../../components/form/AddCategory';
import Radio, { RadioGroup } from '../../components/form/Radio';
import Icon from '../../components/icon/Icon';
import SelectReact from '../../components/form/SelectReact';
import Validation from '../../components/form/Validation';
import FieldWrap from '../../components/form/FieldWrap';
// Utilidades
import priceFormat from '../../utils/priceFormat.util';
// Mocks
import { TypesCompetitions } from '../../mocks/db/categories.db';
// Configuraciónes
import themeConfig from '../../config/theme.config';
import { appPages } from '../../config/pages.config';
// Types
import { TCompetition } from '../../types/competitions';
// Services
import { competitionClient } from '../../services/competitions';
import { sportClient } from '../../services/sports';
import { categoryClient } from '../../services/categories';
// context
import CompetitionContext from '../../context/competitionContext';
import UserContext from '../../context/userContext';
import { useAuth } from '../../context/authContext';
import { useToast } from '../../context/ToastContext';

const CompetitionAdd = () => {
  const competitionContext = useContext(CompetitionContext);
  const userContext = useContext(UserContext);

  const { getToken } = useAuth();
  // estado para guardar la información de los deportes
  const [sports, setSports] = useState<TCompetition[]>([]);
  // estado para guardar la información de las categorías
  const [categories, setCategories] = useState<any[]>([{ name: 'Elige un deporte primero para ver sus categorías disponibles.' }]);
  // estado para habilitar el select de categorías
  const [enabled, setEnabled] = useState<boolean>(false);
  // estado para saber si se está cargando la información
  const [loading, setLoading] = useState<boolean>(false);
  // Obtén la función showToast del contexto de Toast
  const { showToast } = useToast() as { showToast: (message: string, type: string) => void };

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true); // Marca como cargando
      if (sports.length) {
        setLoading(false); // Marca como no cargando
        return;
      } // Evita solicitudes duplicadas
      const token = await getToken(); // Espera a que el token esté disponible
      if (token) {
        await competitionContext.fetchCompetitions(); // Llama a fetchCompetitions si el token está disponible
        const response_sport = await sportClient.Sports(); // Obtiene la información de las competencias
        setSports(response_sport.data); // Guarda la información de las competencias
      }
      setLoading(false); // Marca como no cargando
    };
    fetchData(); // Llama a la función fetchData
  }, [getToken]); // Incluye isLoading como dependencia

  // funcion para cargar las categorías dependiendo del deporte seleccionado
  const fetchCategories = async (sport_guid: string) => {
    if (!sport_guid) return; // Evita llamadas innecesarias

    try {
      const response = await categoryClient.Categories_Sport({ guid: sport_guid }); // Enviar el deporte como filtro

      if (response.code == 200) {
        //validamos si la respuesta viene vacía
        if (response.data == undefined) {
          showToast('Este deporte aún no tiene categorías. Usa el botón verde a la derecha para crear una.', 'info');
          setCategories([{ name: 'Este deporte aún no tiene categorías. Usa el botón verde a la derecha para crear una.' }]); // Limpia las categorías
          return;
        }
        showToast('Categorías cargadas con éxito', 'success');
      }
      showToast('Categorías cargadas con éxito', 'success');
      setCategories(response.data || []); // Guarda las categorías filtradas
    } catch (error) {
      showToast('Error al cargar categorías', 'error');
      setCategories([{ name: 'Este deporte aún no tiene categorías. Usa el botón verde a la derecha para crear una.' }]); // Limpia las categorías
    }
  };

  const validationSchema = Yup.object({
    category_guid: Yup.string().required('Debes seleccionar una categoría'),
    season_guid: Yup.string(),
    sport_guid: Yup.string().required('Debes seleccionar un deporte'),
    type_competition_guid: Yup.string().required('Debes seleccionar un tipo de competencia'),
    name: Yup.string().required('El nombre de la competencia es requerido'),
    description: Yup.string().transform((value) => {
      if (Array.isArray(value)) {
        return value.map((d) => d.children?.map((c) => c.text).join(' ')).join('\n');
      }
      return value; // Si ya es string, no cambia
    }),
    logo_path: Yup.string(),
    teams: Yup.number(),
    players: Yup.string(),
    players_field: Yup.number(),
    start_date: Yup.string(),
    end_date: Yup.string(),
    site_web: Yup.string(),
  });

  const handleSelectChange = (selectedOption) => {
    formik.setFieldValue('category_guid', selectedOption.value);
  };

  const initialValue = '';

  const stringToSlate = (text) => [{ type: 'paragraph', children: [{ text: text || '' }] }];

  const slateToString = (value) => value.map((block) => block.children.map((child) => child.text).join(' ')).join('\n');

  const formik = useFormik({
    initialValues: {
      id: '',
      category_guid: '',
      sport_guid: '',
      type_competition_guid: '',
      name: '',
      description: initialValue,
      logo_path: '',
    },
    validationSchema,
    onSubmit: async (values) => {
      console.log('values:', values);
      try {
        setLoading(true); // Establece el estado de carga a true cuando se envíe el formulario
        // Enviar datos al servidor para guardar
        const response = await competitionClient.AddCompetition(values);

        if (response.code !== 201) {
          showToast('Error al guardar competencia', 'error');
          throw new Error('Error al guardar competencia');
        }
        // Muestra un toast de éxito
        showToast('¡competencia guardado con éxito!', 'success');
        // Recarga la lista de competencias
        competitionContext.fetchCompetitions();
        // Establece el estado de carga a false cuando se haya enviado el formulario
        setLoading(false);
      } catch (error) {
        console.error('Error al guardar competencia:', error);
      }
    },
    enableReinitialize: true, // Esto asegura que los valores del formulario se actualicen cuando `user` cambia
  });

  return (
    <PageWrapper name={'Nueva Competencia'}>
      {/* Modal para agregar nuevos usuarios */}
      <Modal isOpen={userContext?.isModalAdd} setIsOpen={userContext?.setModalAdd}>
        <ModalHeader>Agregar Categoria</ModalHeader>
        <ModalBody>
          <FormAddCategory sport={sports} />
        </ModalBody>
      </Modal>

      <Subheader>
        <SubheaderLeft>
          <Link to={`../${appPages.salesAppPages.subPages.productPage.subPages.listPage.to}`}>
            <Button icon="HeroArrowLeft" className="!px-0">
              Volver a la lista de competencias
            </Button>
          </Link>
          <SubheaderSeparator />
          Nueva Competencia
        </SubheaderLeft>
        <SubheaderRight>
          <Button
            variant="solid"
            onClick={() => {
              formik.handleSubmit();
            }}
          >
            Agregar
          </Button>
        </SubheaderRight>
      </Subheader>

      <Container>
        <div className="grid grid-cols-12 gap-4">
          <div className="col-span-12">
            <h1 className="my-4 font-bold">Agregar una Competencia</h1>
          </div>
          <div className="col-span-12 lg:col-span-3">
            <Card className="top-scroll-offset sticky">
              <CardHeader>
                <CardHeaderChild>
                  <CardTitle>Preview</CardTitle>
                </CardHeaderChild>
                <CardHeaderChild>
                  <Button icon="HeroArrowTopRightOnSquare" />
                </CardHeaderChild>
              </CardHeader>
              <CardBody>
                <div className="flex flex-col gap-4">
                  {formik.values.logo_path && <img src={formik.values.logo_path} alt="" className="aspect-square rounded object-cover" />}
                  <div className="flex flex-wrap items-center justify-between">
                    <div className="p-5 max-w-xs">
                      <h3 className="whitespace-normal break-words">{formik.values.name}</h3>
                    </div>
                    <div className="p-5">
                      {formik.values.sport_guid &&
                        (() => {
                          const selectedSport = sports.find((sport) => sport.guid === formik.values.sport_guid);
                          return (
                            <Icon
                              icon={selectedSport?.icon || 'HeroTennis'}
                              className="h-8 w-8"
                              color={themeConfig.themeColor}
                              colorIntensity={themeConfig.themeColorShade}
                            />
                          );
                        })()}
                    </div>
                    <div className="p-5">
                      {formik.values.type_competition_guid && (
                        <Badge color="blue" variant="solid">
                          {Object.values(TypesCompetitions).find((tc) => tc.guid === formik.values.type_competition_guid)?.name || 'Desconocido'}
                        </Badge>
                      )}
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
          <div className="col-span-12 lg:col-span-9">
            <div className="grid grid-cols-12 gap-4">
              <div className="col-span-12">
                <Card>
                  <CardHeader>
                    <CardHeaderChild>
                      <CardTitle>Información General</CardTitle>
                    </CardHeaderChild>
                  </CardHeader>
                  <CardBody>
                    <div className="grid grid-cols-12 gap-4">
                      <div className="col-span-12">
                        <Label htmlFor="name">Nombre de la Competencia</Label>
                        <Validation
                          isValid={formik.isValid}
                          isTouched={formik.touched.name}
                          invalidFeedback={formik.errors.name}
                          validFeedback="Good"
                        >
                          <Input type="text" id="name" name="name" onChange={formik.handleChange} value={formik.values.name} />
                        </Validation>
                      </div>
                      <div className="col-span-12">
                        <Label htmlFor="sport_guid">Deportes</Label>
                        {Object.keys(sports).length === 0 ? (
                          <div>
                            <div className="flex h-96 items-center justify-center">
                              <PropagateLoader size={15} color="#ffcc00" loading={loading} speedMultiplier={0.8} />
                            </div>
                          </div>
                        ) : (
                          <Validation
                            isValid={formik.isValid}
                            isTouched={formik.touched.sport_guid}
                            invalidFeedback={formik.errors.sport_guid}
                            validFeedback="Good"
                          >
                            <RadioGroup isInline className="grid grid-cols-2 sm:grid-cols-4 xl:grid-cols-6 gap-4">
                              {Object.keys(sports).map((sport) => (
                                <Radio
                                  key={sport}
                                  label={sports[sport].name}
                                  icon={sports[sport].icon}
                                  name="sport_guid"
                                  value={sports[sport].guid}
                                  selectedValue={formik.values.sport_guid}
                                  onChange={(e) => {
                                    formik.handleChange(e); // Actualiza Formik
                                    fetchCategories(e.target.value); // Carga las categorías correspondientes
                                    setEnabled(true);
                                  }}
                                />
                              ))}
                            </RadioGroup>
                          </Validation>
                        )}
                      </div>
                      <div className="col-span-12">
                        <Label htmlFor="type_competition_guid">Tipos de Torneos</Label>
                        <Validation
                          isValid={formik.isValid}
                          isTouched={formik.touched.type_competition_guid}
                          invalidFeedback={formik.errors.type_competition_guid}
                          validFeedback="Good"
                        >
                          <RadioGroup isInline className="grid grid-cols-2 sm:grid-cols-4 xl:grid-cols-5 gap-4">
                            {Object.keys(TypesCompetitions).map((TC) => (
                              <Radio
                                key={TC}
                                label={TypesCompetitions[TC].name}
                                icon={TypesCompetitions[TC].icon}
                                name="type_competition_guid"
                                value={TypesCompetitions[TC].guid}
                                selectedValue={formik.values.type_competition_guid}
                                onChange={formik.handleChange}
                              />
                            ))}
                          </RadioGroup>
                        </Validation>
                      </div>
                      <div className="col-span-12 lg:col-span-6">
                        <Label htmlFor="id">Categorias</Label>
                        <div className="grid grid-cols-12 gap-4">
                          <div className="col-span-10 lg:col-span-10">
                            <Validation
                              isValid={formik.isValid}
                              isTouched={formik.touched.category_guid}
                              invalidFeedback={formik.errors.category_guid}
                              validFeedback="Good"
                            >
                              <SelectReact
                                className="w-full"
                                options={categories.map((category) => ({
                                  value: category.guid,
                                  label: category.name,
                                }))}
                                id="category_guid"
                                name="category_guid"
                                onChange={handleSelectChange}
                                disabled={!enabled}
                                placeholder={!enabled ? 'Selecciona un deporte primero' : ''}
                                value={categories.find((category) => category.guid === formik.values.category_guid)}
                              />
                            </Validation>
                          </div>
                          <div className="col-span-2 lg:col-span-2">
                            <Button
                              icon="MdAdd"
                              color="lime"
                              variant="solid"
                              onClick={() => {
                                userContext?.setModalAdd(true);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-span-12">
                        <Label htmlFor="description">Descripción</Label>
                        <Validation
                          isValid={formik.isValid}
                          isTouched={Array.isArray(formik.touched.description) ? false : !!formik.touched.description}
                          invalidFeedback={formik.errors.description ? String(formik.errors.description) : ''}
                          validFeedback="Good"
                        >
                          <RichText
                            id="description"
                            value={stringToSlate(formik.values.description)} // Convertimos el string a formato Slate.js
                            handleChange={(value) => {
                              formik.setFieldValue('description', slateToString(value)); // Guardamos como string en Formik
                            }}
                          />
                        </Validation>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
              <div className="col-span-12">
                <Card>
                  <CardHeader>
                    <CardHeaderChild>
                      <CardTitle>Foto de perfil</CardTitle>
                    </CardHeaderChild>
                  </CardHeader>
                  <CardBody>
                    <div
                      className={classNames(
                        'mt-2',
                        'flex justify-center',
                        'rounded-lg',
                        'border border-dashed border-zinc-500/25',
                        'px-6 py-10',
                        'dark:border-zinc-500/50',
                      )}
                    >
                      <div className="text-center">
                        <Icon
                          icon="HeroPhoto"
                          className="mx-auto h-12 w-12"
                          color={themeConfig.themeColor}
                          colorIntensity={themeConfig.themeColorShade}
                        />
                        <div className="mt-4 flex text-sm leading-6 text-gray-500">
                          <label
                            htmlFor="logo_path"
                            className={classNames(
                              'relative',
                              'cursor-pointer',
                              'rounded-md',
                              'font-semibold',
                              'text-blue-500',
                              'focus-within:outline-none',
                              'focus-within:ring-2 focus-within:ring-blue-600 focus-within:ring-offset-2 focus-within:ring-offset-transparent',
                              'hover:text-blue-600',
                              themeConfig.transition,
                            )}
                          >
                            <span>Subir un archivo</span>
                            <input id="logo_path" name="logo_path" type="file" className="sr-only" />
                          </label>
                          <span className="pl-1">o arrastrar y soltar</span>
                        </div>
                        <p className="text-xs leading-5 text-gray-500">PNG, JPG, GIF hasta 10 MB</p>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </div>
          </div>
        </div>
      </Container>
      {/* Mostrar el toast si el estado lo indica */}
      <ToastContainer stacked draggable />
    </PageWrapper>
  );
};

export default CompetitionAdd;
