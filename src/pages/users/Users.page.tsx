// src/pages/UsersPage.tsx
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {
  createColumnHelper,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
  ColumnFiltersState,
  FilterFnOption,
} from '@tanstack/react-table';
// config
import PropagateLoader from 'react-spinners/PropagateLoader';
import { adminPages } from '../../config/pages.config';
// mocks
import { TUser } from '../../mocks/db/users.db';
// templates
import TableTemplate, { TableCardFooterTemplate } from '../../templates/common/TableParts.template';
// components
import PageWrapper from '../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../components/layouts/Container/Container';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../components/ui/Card';
import Button from '../../components/ui/Button';
import Icon from '../../components/icon/Icon';
import Input from '../../components/form/Input';
import Badge from '../../components/ui/Badge';
import Dropdown, { DropdownItem, DropdownMenu, DropdownToggle } from '../../components/ui/Dropdown';
import Subheader, { SubheaderLeft, SubheaderRight } from '../../components/layouts/Subheader/Subheader';
import FieldWrap from '../../components/form/FieldWrap';
import Avatar from '../../components/Avatar';
import Tooltip from '../../components/ui/Tooltip';
import Modal, { ModalBody, ModalFooter, ModalFooterChild, ModalHeader } from '../../components/ui/Modal';
import FormAddUser from '../../components/form/AddUser';
// utils
import { exportPDF, exportExcel, exportCSV } from '../../utils/export.utils';
// context
import UserContext from '../../context/userContext';
import { useAuth } from '../../context/authContext';
// Services
import { userClient } from '../../services/users';
import { ToastContainer } from 'react-toastify';

const columnHelper = createColumnHelper<TUser>();

const editLinkPath = `../${adminPages.editUsersPage.to}/`;

const UsersPage = () => {
  const userContext = useContext(UserContext);
  const { getToken } = useAuth();
  // estado para el ordenamiento de la tabla
  const [sorting, setSorting] = useState<SortingState>([]);
  // estado para el filtro global
  const [globalFilter, setGlobalFilter] = useState<string>('');
  // estado para saber si se está cargando la información
  const [isLoading, setIsLoading] = useState<boolean>(false);
  // estado para abrir y cerrar el modal de eliminar usuario
  const [isModalOpen, setModal] = useState<boolean>(false);
  // estado para guardar el guid del usuario a eliminar
  const [guidDelete, setGuidDelete] = useState<string>('');

  const [columnFilters, setColumnFilters] = useState<ColumnFiltersState>([]); // can set initial column filter state here

  const columns = [
    columnHelper.accessor('image', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <Avatar
            src={info.row.original.image_path ? info.row.original.image_path : info.row.original.picture}
            name={`${info.row.original.first_name} ${info.row.original.last_name}`}
            className="!aspect-[9/12] !w-12 2xl:!w-20"
            rounded="rounded"
          />
        </Link>
      ),
      header: 'Imagen',
      footer: 'Imagen',
      enableGlobalFilter: false,
      enableColumnFilter: false,
      enableSorting: false,
      exportable: false,
    }),
    columnHelper.accessor('username', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold">{`${info.row.original.first_name} ${info.row.original.last_name}`}</div>
          <div className="text-sm">@{info.getValue()}</div>
        </Link>
      ),
      header: 'Usuario',
      footer: 'Usuario',
      enableGlobalFilter: true,
      enableColumnFilter: true,
      exportable: true,
    }),
    columnHelper.accessor('email', {
      cell: (info) => (
        <a href={`mailto:${info.getValue()}`} className="inline min-w-[12rem] max-w-full whitespace-normal break-words">
          <span className="inline">{info.getValue()}</span>
          {info.row.original.isVerified && <Icon icon="HeroCheckBadge" color="blue" size="text-3xl" className="inline-block ml-2" />}
        </a>
      ),
      header: 'Correo',
      footer: 'Correo',
      enableGlobalFilter: true,
      enableColumnFilter: true,
      enableResizing: true,
      size: 500,
      exportable: true,
    }),
    columnHelper.accessor('role.name', {
      cell: (info) => <span className="min-w-[8rem] max-w-full">{info.getValue()}</span>,
      header: 'Rol',
      footer: 'Rol',
      enableGlobalFilter: false,
      enableColumnFilter: false,
      enableSorting: false,
      exportable: true,
    }),
    columnHelper.accessor('google_id', {
      cell: (info) => (
        <div className="flex items-center gap-2">
          {info.row.original?.google_id && (
            <Tooltip text="Google">
              <Icon size="text-xl" icon="CustomGoogle" />
            </Tooltip>
          )}
          {info.row.original?.facebook_id && (
            <Tooltip text="Facebook">
              <Icon size="text-xl" icon="CustomFacebook" />
            </Tooltip>
          )}
          {info.row.original.socialAuth?.apple && (
            <Tooltip text="Apple">
              <Icon size="text-xl" icon="CustomApple" />
            </Tooltip>
          )}
        </div>
      ),
      header: 'Red Social',
      footer: 'Red Social',
      enableColumnFilter: false,
      enableGlobalFilter: false,
      enableSorting: false,
      exportable: false,
    }),
    columnHelper.accessor('actions', {
      cell: (info) => (
        <div className="flex items-center gap-2">
          <Link to={`${editLinkPath}${info.row.original.guid}`}>
            <Tooltip text="Editar">
              <Button variant="outline" icon="HeroPencil" color="amber" />
            </Tooltip>
          </Link>
          <Tooltip text="Eliminar">
            <Button
              onClick={() => {
                setModal(true);
                setGuidDelete(info.row.original.guid);
              }}
              color="red"
              icon="HeroTrash"
              variant="outline"
            />
          </Tooltip>
        </div>
      ),
      header: 'Acciones',
      footer: 'Acciones',
      enableColumnFilter: false,
      enableGlobalFilter: false,
      enableSorting: false,
      exportable: false,
    }),
  ];

  const table = useReactTable({
    data: userContext?.users || [],
    columns,
    state: {
      sorting,
      globalFilter,
      columnFilters,
    },
    onColumnFiltersChange: setColumnFilters,
    onSortingChange: setSorting,
    enableGlobalFilter: true,
    onGlobalFilterChange: setGlobalFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: { pageSize: 5 },
    },
    globalFilterFn: 'fuzzy' as FilterFnOption<TUser>, // or choose the desired filter function type
    defaultColumn: {
      size: 300,
      minSize: 200,
    },
  });

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true); // Marca como cargando
      if (userContext?.users.length) {
        setIsLoading(false); // Marca como no cargando
        return;
      } // Evita solicitudes duplicadas
      const token = await getToken(); // Espera a que el token esté disponible
      if (token && userContext?.fetchUsers) {
        await userContext.fetchUsers(); // Llama a fetchUsers si el token está disponible
      }
      setIsLoading(false); // Marca como no cargando
    };
    fetchData(); // Llama a la función fetchData
  }, [getToken]); // Incluye isLoading como dependencia

  // funcion para eliminar usuario
  const handleDeleteUser = async (guid: string) => {
    console.log('Usuario eliminado:', guid);
    try {
      const response = await userClient.delete({ guid });
      console.log('Usuario eliminado:', response);
      userContext?.fetchUsers();
    } catch (error) {
      console.error('Error eliminando usuario:', error);
    }
  };

  return (
    <PageWrapper name="Usuarios">
      {/* Modal para eliminar usuarios */}
      <Modal isOpen={isModalOpen} setIsOpen={setModal}>
        <ModalHeader>Eliminar Usuario</ModalHeader>
        <ModalBody>
          ¿Estás seguro de eliminar a{' '}
          {`${userContext?.users.find((user) => user.guid === guidDelete)?.first_name} ${userContext?.users.find((user) => user.guid === guidDelete)
            ?.last_name}`}
          ?
        </ModalBody>
        <ModalFooter>
          <ModalFooterChild>
            <Button onClick={() => setModal(false)} variant="outline">
              Cancelar
            </Button>
          </ModalFooterChild>
          <ModalFooterChild>
            <Button
              onClick={() => {
                setModal(false);
                handleDeleteUser(guidDelete);
              }}
              variant="solid"
            >
              Eliminar
            </Button>
          </ModalFooterChild>
        </ModalFooter>
      </Modal>

      {/* Modal para agregar nuevos usuarios */}
      <Modal isOpen={userContext?.isModalAdd} setIsOpen={userContext?.setModalAdd}>
        <ModalHeader>Agregar Usuario</ModalHeader>
        <ModalBody>
          <FormAddUser />
        </ModalBody>
      </Modal>

      <Subheader>
        <SubheaderLeft>
          <FieldWrap
            firstSuffix={<Icon className="mx-2" icon="HeroMagnifyingGlass" />}
            lastSuffix={globalFilter && <Icon icon="HeroXMark" color="red" className="mx-2 cursor-pointer" onClick={() => setGlobalFilter('')} />}
          >
            <Input id="search" name="search" placeholder="Buscar..." value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} />
          </FieldWrap>
        </SubheaderLeft>
        <SubheaderRight>
          <Button
            variant="solid"
            icon="HeroPlus"
            onClick={() => {
              userContext?.setModalAdd(true);
            }}
          >
            Agregar Usuario
          </Button>
        </SubheaderRight>
      </Subheader>
      <Container>
        <Card className="h-full">
          <CardHeader>
            <CardHeaderChild>
              <CardTitle>Todos los Usarios</CardTitle>
              <Badge variant="outline" className="border-transparent px-4" rounded="rounded-full">
                {table.getFilteredRowModel().rows.length} Registros
              </Badge>
            </CardHeaderChild>
            <CardHeaderChild>
              <Dropdown>
                <DropdownToggle>
                  <Button icon="HeroRocketLaunch">Exportar</Button>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => exportCSV(users, columns, 'users.csv')} icon="HeroDocumentDownload">
                    Exportar CSV
                  </DropdownItem>
                  <DropdownItem onClick={() => exportExcel(users, columns, 'users.xlsx')} icon="HeroDocumentDownload">
                    Exportar Excel
                  </DropdownItem>
                  <DropdownItem onClick={() => exportPDF(users, columns, 'users.pdf')} icon="HeroDocumentDownload">
                    Exportar PDF
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </CardHeaderChild>
          </CardHeader>

          <CardBody className="overflow-auto">
            {isLoading ? (
              <div className="flex h-96 items-center justify-center">
                <PropagateLoader size={15} color="#ffcc00" loading={isLoading} speedMultiplier={0.8} />
              </div>
            ) : (
              <TableTemplate className="table-fixed max-md:min-w-[100rem]" table={table} />
            )}
          </CardBody>
          <TableCardFooterTemplate table={table} />
        </Card>
      </Container>

      {/* Mostrar el toast si el estado lo indica */}
      <ToastContainer stacked draggable />
    </PageWrapper>
  );
};

export default UsersPage;
