import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';
// config
import PropagateLoader from 'react-spinners/PropagateLoader';
import { adminPages } from '../../config/pages.config';
// mocks
import rolesDb from '../../mocks/db/roles.db';
// components
import Card, { CardBody, CardFooter, CardFooterChild, CardHeader, CardHeaderChild, CardTitle } from '../../components/ui/Card';
import Subheader, { SubheaderLeft, SubheaderRight, SubheaderSeparator } from '../../components/layouts/Subheader/Subheader';
import PageWrapper from '../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../components/layouts/Container/Container';
import Avatar from '../../components/Avatar';
import Button from '../../components/ui/Button';
import Label from '../../components/form/Label';
import Input from '../../components/form/Input';
import Select from '../../components/form/Select';
import FieldWrap from '../../components/form/FieldWrap';
import Icon from '../../components/icon/Icon';
import Checkbox from '../../components/form/Checkbox';
import Badge from '../../components/ui/Badge';
// hooks
import useSaveBtn from '../../hooks/useSaveBtn';
// services
import { useUserContext } from '../../context/userContext';
import { useAuth } from '../../context/authContext';
import { userClient } from '../../services/users';
import { ToastContainer } from 'react-toastify';
import { TUser } from '../../types/users';
import { useValidation } from '../../utils/validation.utils'; // Importa las funciones de validación
import { useToast } from '../../context/ToastContext';

const EditUserPage = () => {
  const { validateEmailUniquenes, validatePhoneUniquenes, validateUsernameUniquenes } = useValidation();
  const { guid } = useParams();
  const [user, setUser] = useState<TUser | null>(null);
  const [isSaving, setIsSaving] = useState<boolean>(false);
  const { i18n } = useTranslation();
  const { users, fetchUsers } = useUserContext() ?? {}; // Obtén el estado global de usuarios
  const { getToken } = useAuth();
  const [loading, setLoading] = useState(false);
  const { showToast } = useToast() as { showToast: (message: string, type: string) => void }; // Obtén la función showToast del contexto de Toast

  useEffect(() => {
    const fetchData = async () => {
      if (users?.length === 0) {
        const token = await getToken();
        // Si no hay usuarios en el estado global, realiza una solicitud para obtenerlos
        if (fetchUsers && token) {
          await fetchUsers();
        }
      }
    };

    fetchData();
  }, [fetchUsers, users?.length ?? 0, getToken]);

  useEffect(() => {
    if (guid) {
      const foundUser = users?.find((user) => user.guid === guid);
      setUser(foundUser || null);
    }
  }, [guid, users]);

  const isNewItem = !guid;

  const validationSchema = Yup.object({
    guid: Yup.string().required('El GUID del usuario es obligatorio'),
    username: Yup.string()
      .min(3, 'El nombre de usuario debe tener al menos 3 caracteres')
      .max(50, 'El nombre de usuario debe tener máximo 50 caracteres')
      .required('El nombre de usuario es obligatorio')
      .matches(/^[a-zA-Z0-9_]+$/, 'El nombre de usuario solo puede contener letras, números y guiones bajos')
      .test('username-unique', 'El nombre de usuario ya está en uso', async function (value) {
        try {
          const isUnique = await validateUsernameUniquenes(value, user?.username || '');
          return isUnique;
        } catch (error) {
          console.error('Error en validación de unicidad del nombre de usuario:', error);
          return false; // o puedes retornar `true` si quieres evitar bloquear el envío por error en la validación
        }
      }),
    email: Yup.string()
      .email('Debes ingresar un correo válido')
      .required('El correo es obligatorio')
      .test('email-unique', 'El correo ya está en uso', async function (value) {
        try {
          const isUnique = await validateEmailUniquenes(value, user?.email || '');
          return isUnique;
        } catch (error) {
          return this.createError({ message: error.message });
        }
      }),
    phone: Yup.string()
      .matches(/^[0-9]+$/, 'El teléfono solo debe contener números')
      .min(10, 'El teléfono debe tener al menos 10 dígitos')
      .max(10, 'El teléfono debe tener máximo 10 dígitos')
      .test('phone-unique', 'El teléfono ya está en uso', async function (value) {
        try {
          const isUnique = await validatePhoneUniquenes(value, user?.phone || '');
          return isUnique;
        } catch (error) {
          return this.createError({ message: error.message });
        }
      }),
    first_name: Yup.string().required('El nombre es obligatorio'),
    last_name: Yup.string(),
    role_guid: Yup.string().required('Debes seleccionar un rol'),
    country: Yup.string(),
    state: Yup.string(),
    city: Yup.string(),
    facebook: Yup.string(),
    instagram: Yup.string(),
  });

  const formik = useFormik({
    initialValues: {
      guid: user?.guid || '',
      username: user?.username || '',
      email: user?.email || '',
      first_name: user?.first_name || '',
      last_name: user?.last_name || '',
      role_guid: user?.role_guid || '',
      country: user?.country || 'México',
      state: user?.state || '',
      city: user?.city || '',
      facebook: user?.facebook || '',
      instagram: user?.instagram || '',
      twoFactorAuth: user?.twoFactorAuth || false,
      weeklyNewsletter: user?.weeklyNewsletter || false,
      lifecycleEmails: user?.lifecycleEmails || false,
      promotionalEmails: user?.promotionalEmails || false,
      productUpdates: user?.productUpdates || false,
    },
    validationSchema,
    onSubmit: async (values) => {
      try {
        setLoading(true); // Establece el estado de carga a true cuando se envíe el formulario
        // Enviar datos al servidor (debes implementar esta función)
        const response = await userClient.update({ guid: user?.guid || '', ...values });

        if (response.code !== 201) {
          showToast('Error al guardar usuario', 'error');
          throw new Error('Error al guardar usuario');
        }
        //agergamos un toast de exito para que se muestre cuando se envia el correo
        showToast('¡Usuario guardado con éxito!', 'success'); // Muestra un toast de error'
        // Recarga los usuarios para mostrar el nuevo usuario
        fetchUsers();
        // Establece el estado de carga a false cuando se haya enviado el formulario
        setLoading(false);
      } catch (error) {
        console.error('Error al guardar usuario:', error);
      }
    },
    enableReinitialize: true, // Esto asegura que los valores del formulario se actualicen cuando `user` cambia
  });

  const { saveBtnText, saveBtnColor, saveBtnDisable } = useSaveBtn({
    isNewItem,
    isSaving,
    isDirty: formik.dirty,
  });

  if (user === null) {
    return (
      <div className="flex h-96 items-center justify-center">
        <PropagateLoader size={15} color="#ffcc00" speedMultiplier={0.8} />
      </div>
    );
  }

  return (
    <PageWrapper name={'Editar Usuario: ' + user?.first_name + ' ' + user?.last_name}>
      <Subheader>
        <SubheaderLeft>
          <Link to={`../${adminPages.usersPage.to}`}>
            <Button icon="HeroArrowLeft" className="!px-0">
              Volver a la lista de usuarios
            </Button>
          </Link>
          <SubheaderSeparator />
          {isNewItem ? (
            'Add New User'
          ) : (
            <>
              {`${user?.first_name} ${user?.last_name}`}{' '}
              <Badge color="blue" variant="outline" rounded="rounded-full" className="border-transparent">
                Editar Usuario
              </Badge>
            </>
          )}
        </SubheaderLeft>
        <SubheaderRight>
          <Button icon="HeroServer" variant="solid" color={saveBtnColor} isDisable={saveBtnDisable} onClick={() => formik.handleSubmit()}>
            {saveBtnText}
          </Button>
        </SubheaderRight>
      </Subheader>
      <Container className="flex shrink-0 grow basis-auto flex-col pb-0">
        <div className="flex h-full flex-wrap content-start">
          <div className="mb-4 grid w-full grid-cols-12 gap-4">
            <div className="col-span-12 flex flex-col gap-4 xl:col-span-6">
              <Card>
                <CardBody>
                  <div className="flex w-full gap-4">
                    <div className="flex-shrink-0">
                      <Avatar
                        src={user?.image_path || user?.picture || user?.image?.thumb}
                        className="!w-24"
                        name={`${user?.first_name} ${user?.last_name}`}
                      />
                    </div>
                    <div className="flex grow items-center">
                      <div>
                        <div className="w-full text-2xl font-semibold">
                          {user?.first_name} {user?.last_name}
                        </div>

                        <div className="w-full text-zinc-500">{user?.email}</div>
                      </div>
                    </div>
                  </div>
                </CardBody>
              </Card>
              <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>
                      <div>
                        <div>Configuraciones de la cuenta</div>
                        <div className="text-lg font-normal text-zinc-500">Aquí puede cambiar la información de la cuenta de usuario</div>
                      </div>
                    </CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="grid grid-cols-12 gap-4">
                    <div className="col-span-12 lg:col-span-6">
                      <Label htmlFor="username">Username*</Label>
                      <Input
                        id="username"
                        name="username"
                        onChange={formik.handleChange}
                        value={formik.values.username}
                        autoComplete="username"
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched.username && formik.errors.username ? <div className="text-red-500">{formik.errors.username}</div> : null}
                    </div>
                    <div className="col-span-12 lg:col-span-6">
                      <Label htmlFor="email">Correo*</Label>
                      <Input
                        id="email"
                        name="email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                        autoComplete="email"
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched.email && formik.errors.email ? <div className="text-red-500">{formik.errors.email}</div> : null}
                    </div>
                    <div className="col-span-12 lg:col-span-6">
                      <Label htmlFor="first_name">Nombre*</Label>
                      <Input
                        id="first_name"
                        name="first_name"
                        onChange={formik.handleChange}
                        value={formik.values.first_name}
                        autoComplete="given-name"
                        autoCapitalize="words"
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched.first_name && formik.errors.first_name ? <div className="text-red-500">{formik.errors.first_name}</div> : null}
                    </div>
                    <div className="col-span-12 lg:col-span-6">
                      <Label htmlFor="last_name">Apellido</Label>
                      <Input
                        id="last_name"
                        name="last_name"
                        onChange={formik.handleChange}
                        value={formik.values.last_name}
                        autoComplete="family-name"
                        autoCapitalize="words"
                        onBlur={formik.handleBlur}
                      />
                      {formik.touched.last_name && formik.errors.last_name ? <div className="text-red-500">{formik.errors.last_name}</div> : null}
                    </div>

                    <div className="col-span-12">
                      <Label htmlFor="role_guid">Rol*</Label>
                      <Select
                        name="role_guid"
                        onChange={formik.handleChange}
                        value={formik.values.role_guid}
                        placeholder="Selecciona un rol*"
                        onBlur={formik.handleBlur}
                      >
                        {rolesDb.map((role) => (
                          <option key={role.id} value={role.id}>
                            {role.name}
                          </option>
                        ))}
                      </Select>
                      {formik.touched.role_guid && formik.errors.role_guid ? <div className="text-red-500">{formik.errors.role_guid}</div> : null}
                    </div>
                  </div>
                </CardBody>
              </Card>
              <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>
                      <div>
                        <div>Perfiles sociales</div>
                        <div className="text-lg font-normal text-zinc-500">Aquí puedes configurar los perfiles sociales de los usuarios.</div>
                      </div>
                    </CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="grid grid-cols-12 gap-4">
                    <div className="col-span-12">
                      <Label htmlFor="facebook">Facebook</Label>
                      <FieldWrap firstSuffix="https://facebook.com/">
                        <Input
                          id="facebook"
                          name="facebook"
                          onChange={formik.handleChange}
                          value={formik.values.facebook}
                          placeholder="username"
                          onBlur={formik.handleBlur}
                        />
                      </FieldWrap>
                    </div>

                    <div className="col-span-12">
                      <Label htmlFor="instagram">Instagram</Label>
                      <FieldWrap firstSuffix="https://instagram.com/">
                        <Input
                          id="instagram"
                          name="instagram"
                          onChange={formik.handleChange}
                          value={formik.values.instagram}
                          placeholder="username"
                          onBlur={formik.handleBlur}
                        />
                      </FieldWrap>
                    </div>
                  </div>
                </CardBody>
              </Card>
              {/* <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>
                      <div>
                        <div>Cambiar la contraseña</div>
                        <div className="text-lg font-normal text-zinc-500">Aquí puedes configurar tu nueva contraseña</div>
                      </div>
                    </CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="grid grid-cols-12 gap-4">
                    <div className="col-span-12">
                      <Label htmlFor="oldPassword">Contraseña anterior</Label>
                      <FieldWrap
                        lastSuffix={
                          <Icon
                            className="mx-2"
                            icon={passwordShowStatus ? 'HeroEyeSlash' : 'HeroEye'}
                            onClick={() => {
                              setPasswordShowStatus(!passwordShowStatus);
                            }}
                          />
                        }
                      >
                        <Input
                          type={passwordShowStatus ? 'text' : 'password'}
                          id="oldPassword"
                          name="oldPassword"
                          onChange={formik.handleChange}
                          value={formik.values.oldPassword}
                          autoComplete="current-password"
                        />
                      </FieldWrap>
                    </div>
                    <div className="col-span-12">
                      <Label htmlFor="newPassword">Nueva contraseña</Label>
                      <FieldWrap
                        lastSuffix={
                          <Icon
                            className="mx-2"
                            icon={passwordNewShowStatus ? 'HeroEyeSlash' : 'HeroEye'}
                            onClick={() => {
                              setPasswordNewShowStatus(!passwordNewShowStatus);
                            }}
                          />
                        }
                      >
                        <Input
                          type={passwordNewShowStatus ? 'text' : 'password'}
                          id="newPassword"
                          name="newPassword"
                          onChange={formik.handleChange}
                          value={formik.values.newPassword}
                          autoComplete="new-password"
                        />
                      </FieldWrap>
                    </div>
                    <div className="col-span-12">
                      <Label htmlFor="newPasswordConfirmation">Confirmación de nueva contraseña</Label>
                      <FieldWrap
                        lastSuffix={
                          <Icon
                            className="mx-2"
                            icon={passwordNewConfShowStatus ? 'HeroEyeSlash' : 'HeroEye'}
                            onClick={() => {
                              setPasswordNewConfShowStatus(!passwordNewConfShowStatus);
                            }}
                          />
                        }
                      >
                        <Input
                          type={passwordNewConfShowStatus ? 'text' : 'password'}
                          id="newPasswordConfirmation"
                          name="newPasswordConfirmation"
                          onChange={formik.handleChange}
                          value={formik.values.newPasswordConfirmation}
                          autoComplete="new-password"
                        />
                      </FieldWrap>
                    </div>
                  </div>
                </CardBody>
              </Card> */}
            </div>
            <div className="col-span-12 flex flex-col gap-4 xl:col-span-6">
              <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>
                      <div>
                        <div>Configuraciones ubicación</div>
                        <div className="text-lg font-normal text-zinc-500">Aquí puede cambiar la información de la ubicación del usuario</div>
                      </div>
                    </CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="grid grid-cols-12 gap-4">
                    <div className="col-span-12">
                      <Label htmlFor="country">Pais</Label>
                      <Select
                        name="country"
                        onChange={formik.handleChange}
                        value={formik.values.country}
                        placeholder="Selecciona un pais"
                        onBlur={formik.handleBlur}
                        disabled
                      >
                        <option value="México">México</option>
                      </Select>
                    </div>
                    <div className="col-span-12">
                      <Label htmlFor="state">Estado</Label>
                      <Select
                        name="state"
                        onChange={formik.handleChange}
                        value={formik.values.state}
                        placeholder="Selecciona un estado"
                        onBlur={formik.handleBlur}
                      >
                        {rolesDb.map((role) => (
                          <option key={role.id} value={role.id}>
                            {role.name}
                          </option>
                        ))}
                      </Select>
                    </div>
                    <div className="col-span-12">
                      <Label htmlFor="city">Ciudad</Label>
                      <Select
                        name="city"
                        onChange={formik.handleChange}
                        value={formik.values.city}
                        placeholder="Selecciona una ciudad"
                        onBlur={formik.handleBlur}
                      >
                        {rolesDb.map((role) => (
                          <option key={role.id} value={role.id}>
                            {role.name}
                          </option>
                        ))}
                      </Select>
                    </div>
                  </div>
                </CardBody>
              </Card>
              {/* <Card>
								<CardHeader>
									<CardHeaderChild>
										<CardTitle>Autenticación de dos factores</CardTitle>
									</CardHeaderChild>
									<CardHeaderChild>
										{formik.values.twoFactorAuth ? (
											<Badge
												variant='outline'
												className='border-transparent'
												color='emerald'>
												Activado
											</Badge>
										) : (
											<Badge
												variant='outline'
												className='border-transparent'
												color='red'>
												Desactivado
											</Badge>
										)}
										<Checkbox
											variant='switch'
											id='twoFactorAuth'
											name='twoFactorAuth'
											onChange={formik.handleChange}
											checked={formik.values.twoFactorAuth}
										/>
									</CardHeaderChild>
								</CardHeader>
								<CardBody>
									<div className='flex flex-wrap divide-y divide-dashed divide-zinc-500/50 [&>*]:py-4'>
										<div className='flex basis-full gap-4'>
											<div className='flex grow items-center'>
												<div className='w-full text-xl font-semibold'>
													Authenticator App
												</div>
											</div>
											<div className='flex-shrink-0'>
												<Button
													variant='outline'
													isDisable={!formik.values.twoFactorAuth}>
													Configuración
												</Button>
											</div>
										</div>
										<div className='flex basis-full gap-4'>
											<div className='flex grow items-center'>
												<div className='w-full text-xl font-semibold'>
													Llaves de seguridad
												</div>
											</div>
											<div className='flex-shrink-0'>
												<Button
													color='red'
													className='!px-0'
													isDisable={!formik.values.twoFactorAuth}>
													Desactivar
												</Button>
											</div>
										</div>
										<div className='flex basis-full gap-4'>
											<div className='flex grow items-center'>
												<div className='w-full text-xl font-semibold'>
													Número telefónico
												</div>
											</div>
											<div className='flex flex-shrink-0 items-center gap-4'>
												<span className='text-zinc-500'>{user?.phone}</span>
												<Button
													variant='outline'
													color='zinc'
													isDisable={!formik.values.twoFactorAuth}>
													Editar
												</Button>
											</div>
										</div>
									</div>
								</CardBody>
							</Card> */}
              <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>Hoja informativa</CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="flex flex-wrap divide-y divide-dashed divide-zinc-500/50 [&>*]:py-4">
                    <div className="flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <Label htmlFor="weeklyNewsletter" className="!mb-0">
                          <div className="text-xl font-semibold">Boletin semanal</div>
                          <div className="text-zinc-500">Recibe notificaciones sobre artículos, descuentos y nuevos productos.</div>
                        </Label>
                      </div>
                      <div className="flex flex-shrink-0 items-center">
                        <Checkbox
                          variant="switch"
                          id="weeklyNewsletter"
                          name="weeklyNewsletter"
                          onChange={formik.handleChange}
                          checked={formik.values.weeklyNewsletter}
                          onBlur={formik.handleBlur}
                          value={formik.values.weeklyNewsletter.toString()} // Convert boolean to string
                        />
                      </div>
                    </div>
                    <div className="flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <Label htmlFor="lifecycleEmails" className="!mb-0">
                          <div className="text-xl font-semibold">Correos electrónicos de ciclo de vida</div>
                          <div className="text-zinc-500">Reciba ofertas y correos electrónicos personalizados en función de su actividad.</div>
                        </Label>
                      </div>
                      <div className="flex flex-shrink-0 items-center">
                        <Checkbox
                          variant="switch"
                          id="lifecycleEmails"
                          name="lifecycleEmails"
                          onChange={formik.handleChange}
                          checked={formik.values.lifecycleEmails}
                          onBlur={formik.handleBlur}
                        />
                      </div>
                    </div>
                    <div className="flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <Label htmlFor="promotionalEmails" className="!mb-0">
                          <div className="text-xl font-semibold">Correos electrónicos promocionales</div>
                          <div className="text-zinc-500">
                            Reciba ofertas y correos electrónicos personalizados en función de sus pedidos y preferencias.
                          </div>
                        </Label>
                      </div>
                      <div className="flex flex-shrink-0 items-center">
                        <Checkbox
                          variant="switch"
                          id="promotionalEmails"
                          name="promotionalEmails"
                          onChange={formik.handleChange}
                          checked={formik.values.promotionalEmails}
                          onBlur={formik.handleBlur}
                        />
                      </div>
                    </div>
                    <div className="flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <Label htmlFor="productUpdates" className="!mb-0">
                          <div className="text-xl font-semibold">Actualizaciones de productos</div>
                          <div className="text-zinc-500">
                            Si marca esta casilla, podremos notificarle cuando hagamos actualizaciones a los productos que ha descargado o comprado.
                          </div>
                        </Label>
                      </div>
                      <div className="flex flex-shrink-0 items-center">
                        <Checkbox
                          variant="switch"
                          id="productUpdates"
                          name="productUpdates"
                          onChange={formik.handleChange}
                          checked={formik.values.productUpdates}
                          onBlur={formik.handleBlur}
                        />
                      </div>
                    </div>
                  </div>
                </CardBody>
              </Card>
              {/* <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>Sesiones</CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="flex flex-wrap divide-y divide-dashed divide-zinc-500/50 [&>*]:py-4">
                    <div className="group flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <div>
                          <div className="text-xl font-semibold">Chrome</div>
                          <div className="text-zinc-500">MacOS 13.4.1</div>
                        </div>
                        <Button className="invisible group-hover:visible" color="red">
                          Borrar
                        </Button>
                      </div>
                      <div className="flex flex-shrink-0 items-center gap-4">
                        <Icon icon="CustomUSA" size="text-3xl" />
                        <Badge variant="outline" rounded="rounded-full" className="border-transparent">
                          3 Hace horas
                        </Badge>
                      </div>
                    </div>
                    <div className="group flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <div>
                          <div className="text-xl font-semibold">Safari</div>
                          <div className="text-zinc-500">MacOS 13.4.1</div>
                        </div>
                        <Button className="invisible group-hover:visible" color="red">
                          Borrar
                        </Button>
                      </div>
                      <div className="flex flex-shrink-0 items-center gap-4">
                        <Icon icon="CustomUSA" size="text-3xl" />
                        <Badge variant="outline" rounded="rounded-full" className="border-transparent">
                          1 Hace un día
                        </Badge>
                      </div>
                    </div>
                    <div className="group flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <div>
                          <div className="text-xl font-semibold">App</div>
                          <div className="text-zinc-500">iOS 16.5.1</div>
                        </div>
                        <Button className="invisible group-hover:visible" color="red">
                          Borrar
                        </Button>
                      </div>
                      <div className="flex flex-shrink-0 items-center gap-4">
                        <Icon icon="CustomUSA" size="text-3xl" />
                        <Badge variant="outline" rounded="rounded-full" className="border-transparent">
                          3 Hace años
                        </Badge>
                      </div>
                    </div>
                    <div className="group flex basis-full gap-4">
                      <div className="flex grow items-center">
                        <div>
                          <div className="text-xl font-semibold">Safari</div>
                          <div className="text-zinc-500">iPadOS 16.5.1</div>
                        </div>
                        <Button className="invisible group-hover:visible" color="red">
                          Borrar
                        </Button>
                      </div>
                      <div className="flex flex-shrink-0 items-center gap-4">
                        <Icon icon="CustomUSA" size="text-3xl" />
                        <Badge variant="outline" rounded="rounded-full" color="red" className="border-transparent">
                          Caducado
                        </Badge>
                      </div>
                    </div>
                  </div>
                </CardBody>
              </Card> */}
              {/* <Card>
                <CardHeader>
                  <CardHeaderChild>
                    <CardTitle>Conectado</CardTitle>
                  </CardHeaderChild>
                </CardHeader>
                <CardBody>
                  <div className="flex flex-wrap divide-y divide-dashed divide-zinc-500/50 [&>*]:py-4">
                    {user?.socialAuth &&
                      Object.keys(user?.socialAuth).map((i) => {
                        const status = user?.socialAuth[i];
                        return (
                          <div key={i} className="flex basis-full gap-4">
                            <div className="flex grow items-center">
                              <div className="text-xl font-semibold capitalize">{i}</div>
                            </div>
                            <div className="flex flex-shrink-0 items-center gap-4">
                              <Button icon={status ? 'HeroTrash' : 'HeroCog8Tooth'} color={status ? 'red' : 'blue'}>
                                {status ? 'Borrar' : 'Configuración'}
                              </Button>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                </CardBody>
              </Card> */}
            </div>
          </div>
        </div>
        <div className="flex">
          <div className="grid w-full grid-cols-12 gap-4">
            <div className="col-span-12">
              <Card>
                <CardFooter>
                  <CardFooterChild>
                    {isNewItem && (
                      <div className="flex items-center gap-2 text-amber-500">
                        <Icon icon="HeroExclamationTriangle" size="text-2xl" />
                        <span>Aún no guardado</span>
                      </div>
                    )}
                    {!isNewItem && (
                      <div className="flex items-center gap-2">
                        <Icon icon="HeroDocumentCheck" size="text-2xl" />
                        <span className="text-zinc-500">Última modificación realizada:</span>
                        <b>
                          {user?.updated_at
                            ? dayjs(user?.updated_at).format('DD/MM/YYYY HH:mm:ss')
                            : 'Nunca se ha guardado ningún cambio. Verifica si necesitas actualizar la información.'}
                        </b>
                      </div>
                    )}
                  </CardFooterChild>
                  <CardFooterChild>
                    <Button icon="HeroServer" variant="solid" color="blue" isDisable={saveBtnDisable} onClick={() => formik.handleSubmit()}>
                      {saveBtnText}
                    </Button>
                  </CardFooterChild>
                </CardFooter>
              </Card>
            </div>
          </div>
        </div>
      </Container>
      <ToastContainer stacked draggable />
    </PageWrapper>
  );
};

export default EditUserPage;
