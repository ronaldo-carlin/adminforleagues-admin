import { useState, useEffect } from 'react';
import {
  createColumnHelper,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { Link } from 'react-router-dom';
import PageWrapper from '../../../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../../../components/layouts/Container/Container';
import { appPages } from '../../../../config/pages.config';
import { TUser } from '../../../../mocks/db/users.db';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../../../components/ui/Card';
import Button from '../../../../components/ui/Button';
import Icon from '../../../../components/icon/Icon';
import Input from '../../../../components/form/Input';
import TableTemplate, { TableCardFooterTemplate } from '../../../../templates/common/TableParts.template';
import Badge from '../../../../components/ui/Badge';
import Dropdown, { DropdownItem, DropdownMenu, DropdownToggle } from '../../../../components/ui/Dropdown';
import Subheader, { SubheaderLeft, SubheaderRight } from '../../../../components/layouts/Subheader/Subheader';
import FieldWrap from '../../../../components/form/FieldWrap';
import Avatar from '../../../../components/Avatar';
import Tooltip from '../../../../components/ui/Tooltip';

import { exportPDF, exportExcel, exportCSV } from '../../../../utils/export.utils';

import { userClient } from '../../../../services/users';

const columnHelper = createColumnHelper<TUser>();

const editLinkPath = `../${appPages.crmAppPages.subPages.customerPage.subPages.editPageLink.to}/`;

/* en la imagen del usuario debemos de cambiar la imagen, validando, si tiene image_path o si tiene picture, si tiene picture, se debe de cambiar a thumb, si tiene image_path, se debe de cambiar a org */
const columns = [
  columnHelper.accessor('image', {
    cell: (info) => (
      <Link to={`${editLinkPath}${info.row.original.id}`}>
        <Avatar
          src={info.row.original.image_path ? info.row.original.image_path : info.row.original.picture}
          name={`${info.row.original.first_name} ${info.row.original.last_name}`}
          className="!aspect-[9/12] !w-14 2xl:!w-20"
          rounded="rounded"
        />
      </Link>
    ),
    header: 'Imagen',
    footer: 'Imagen',
    enableGlobalFilter: false,
    enableSorting: false,
    exportable: false, // Añade esto para indicar que no se exporte
  }),
  columnHelper.accessor('username', {
    cell: (info) => (
      <Link to={`${editLinkPath}${info.row.original.id}`}>
        <div className="font-bold">{`${info.row.original.first_name} ${info.row.original.last_name}`}</div>
        <div className="text-sm">@{info.getValue()}</div>
      </Link>
    ),
    header: 'Usuario',
    footer: 'Usuario',
    exportable: true,
  }),
  columnHelper.accessor('email', {
    cell: (info) => (
      <a href={`mailto:${info.getValue()}`} className="flex items-center gap-2">
        {info.getValue()}
        {info.row.original.isVerified && <Icon icon="HeroCheckBadge" color="blue" />}
      </a>
    ),
    header: 'Correo',
    footer: 'Correo',
    exportable: true,
  }),
  columnHelper.accessor('role.name', {
    cell: (info) => <span>{info.getValue()}</span>,
    header: 'Rol',
    footer: 'Rol',
    exportable: true,
  }),
  columnHelper.accessor('google_id', {
    cell: (info) => (
      <div className="flex items-center gap-2">
        {info.row.original?.google_id && (
          <Tooltip text="Google">
            <Icon size="text-xl" icon="CustomGoogle" />
          </Tooltip>
        )}
        {info.row.original?.facebook_id && (
          <Tooltip text="Facebook">
            <Icon size="text-xl" icon="CustomFacebook" />
          </Tooltip>
        )}
        {info.row.original.socialAuth?.apple && (
          <Tooltip text="Apple">
            <Icon size="text-xl" icon="CustomApple" />
          </Tooltip>
        )}
      </div>
    ),
    header: 'Red Social',
    footer: 'Red Social',
    exportable: false,
  }),
];

const CustomerListPage = () => {
  const [sorting, setSorting] = useState<SortingState>([]);
  const [globalFilter, setGlobalFilter] = useState<string>('');
  // creamos un estado con los datos de los usuarios
  const [data, setData] = useState<TUser[]>([]);

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
      globalFilter,
    },
    onSortingChange: setSorting,
    enableGlobalFilter: true,
    onGlobalFilterChange: setGlobalFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: { pageSize: 5 },
    },
    // debugTable: true,
  });

  // creamos una petición a la API para obtener los datos
  const fetchData = async () => {
    try {
      const response = await userClient.users();
      console.log('response:', response);
      setData(response.data);
    } catch (error) {
      console.error('Error en la petición:', error);
    }
  };

  useEffect(() => {
    fetchData(); // Llama a fetchData cuando el componente se monta
  }, []);

  return (
    <PageWrapper name="Customer List">
      <Subheader>
        <SubheaderLeft>
          <FieldWrap
            firstSuffix={<Icon className="mx-2" icon="HeroMagnifyingGlass" />}
            lastSuffix={
              globalFilter && (
                <Icon
                  icon="HeroXMark"
                  color="red"
                  className="mx-2 cursor-pointer"
                  onClick={() => {
                    setGlobalFilter('');
                  }}
                />
              )
            }
          >
            <Input id="example" name="example" placeholder="Search..." value={globalFilter ?? ''} onChange={(e) => setGlobalFilter(e.target.value)} />
          </FieldWrap>
        </SubheaderLeft>
        <SubheaderRight>
          <Link to={`${editLinkPath}new`}>
            <Button variant="solid" icon="HeroPlus">
              New Customer
            </Button>
          </Link>
        </SubheaderRight>
      </Subheader>
      <Container>
        <Card className="h-full">
          <CardHeader>
            <CardHeaderChild>
              <CardTitle>Todos los Usarios</CardTitle>
              <Badge variant="outline" className="border-transparent px-4" rounded="rounded-full">
                {table.getFilteredRowModel().rows.length} items
              </Badge>
            </CardHeaderChild>
            <CardHeaderChild>
              <Dropdown>
                <DropdownToggle>
                  <Button icon="HeroRocketLaunch">Actions</Button>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => exportCSV(data, columns, 'users.csv')} icon="HeroDocumentDownload">
                    Exportar CSV
                  </DropdownItem>
                  <DropdownItem onClick={() => exportExcel(data, columns, 'users.xlsx')} icon="HeroDocumentDownload">
                    Exportar Excel
                  </DropdownItem>
                  <DropdownItem onClick={() => exportPDF(data, columns, 'users.pdf')} icon="HeroDocumentDownload">
                    Exportar PDF
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </CardHeaderChild>
          </CardHeader>

          <CardBody className="overflow-auto">
            <TableTemplate className="table-fixed max-md:min-w-[70rem]" table={table} />
          </CardBody>
          <TableCardFooterTemplate table={table} />
        </Card>
      </Container>
    </PageWrapper>
  );
};

export default CustomerListPage;
