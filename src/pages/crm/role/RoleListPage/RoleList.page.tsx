import React, { useContext, useEffect, useState } from 'react';
import {
  createColumnHelper,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { Link } from 'react-router-dom';
// Componentes
import PageWrapper from '../../../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../../../components/layouts/Container/Container';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../../../components/ui/Card';
import Button from '../../../../components/ui/Button';
import Badge from '../../../../components/ui/Badge';
// Configuraciónes
import { appPages } from '../../../../config/pages.config';
// Templates
import TableTemplate, { TableCardFooterTemplate } from '../../../../templates/common/TableParts.template';
// Context
import { useAuth } from '../../../../context/authContext';
import AccessControlContext from '../../../../context/accessControlContext';
// Types
import { TAccessControls } from '../../../../types/access_control';
import Tooltip from '../../../../components/ui/Tooltip';

const columnHelper = createColumnHelper<TAccessControls>();

const editLinkPath = `../${appPages.crmAppPages.subPages.rolePage.subPages.editPageLink.to}/`;

const RoleListPage = () => {
  // Obtiene el contexto de los accesos
  const accessControlContext = useContext(AccessControlContext);
  // Obtiene el contexto del token de autenticación
  const { getToken } = useAuth();
  const [sorting, setSorting] = useState<SortingState>([]);
  const [globalFilter, setGlobalFilter] = useState<string>('');
  // estado para saber si se está cargando la información
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true); // Marca como cargando
      if (accessControlContext?.accessControls.length) {
        setIsLoading(false); // Marca como no cargando
        return;
      } // Evita solicitudes duplicadas
      const token = await getToken(); // Espera a que el token esté disponible
      if (token && accessControlContext?.fetchAccessControls) {
        await accessControlContext.fetchAccessControls(); // Obtiene los accesos
      }
      setIsLoading(false); // Marca como no cargando
    };
    fetchData(); // Llama a la función fetchData
  }, [getToken]); // Incluye isLoading como dependencia

  const columns = [
    columnHelper.accessor('role.name', {
      cell: (info) => (
        <Link to={`${editLinkPath}${info.row.original.guid}`}>
          <div className="font-bold">{info.row.original.role.name}</div>
        </Link>
      ),
      header: 'Nombre del Rol',
      footer: 'Nombre del Rol',
    }),
    columnHelper.display({
      cell: (info) => (
        <div className="flex gap-2">
          <Badge variant="outline" rounded="rounded-full" className="border-transparent">
            {/* recorremos todos los modulos */}
            {info.row.original.modules.map((module) => (
              <span key={module.guid} className="me-2">
                {module.name}
                {/* recorremos todos los permisos y los separamos con una coma */}
                {module.permissions.map((permission) => (
                  <b key={permission.guid} className="ms-2">
                    {permission.id}
                  </b>
                ))}
              </span>
            ))}
          </Badge>
        </div>
      ),
      header: 'Módulos y Permisos',
      footer: 'Módulos y Permisos',
    }),
    columnHelper.display({
      cell: (info) => (
        <div className="flex items-center gap-2">
          <Link to={`${editLinkPath}${info.row.original.role.guid}`}>
            <Tooltip text="Editar">
              <Button variant="outline" icon="HeroPencil" color="amber" />
            </Tooltip>
          </Link>
        </div>
      ),
      header: 'Acciones',
      footer: 'Acciones',
    }),
  ];

  const table = useReactTable({
    data: accessControlContext?.accessControls ?? [],
    columns,
    state: {
      sorting,
      globalFilter,
    },
    onSortingChange: setSorting,
    enableGlobalFilter: true,
    onGlobalFilterChange: setGlobalFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: { pageSize: 5 },
    },
    // debugTable: true,
  });

  return (
    <PageWrapper name="Roles List">
      {/* <Subheader>
        <SubheaderLeft>
          <FieldWrap
            firstSuffix={<Icon className="mx-2" icon="HeroMagnifyingGlass" />}
            lastSuffix={
              globalFilter && (
                <Icon
                  icon="HeroXMark"
                  color="red"
                  className="mx-2 cursor-pointer"
                  onClick={() => {
                    setGlobalFilter('');
                  }}
                />
              )
            }
          >
            <Input id="example" name="example" placeholder="Search..." value={globalFilter ?? ''} onChange={(e) => setGlobalFilter(e.target.value)} />
          </FieldWrap>
        </SubheaderLeft>
        <SubheaderRight>
          <Button variant="solid" icon="HeroPlus" isDisable>
            New Role
          </Button>
        </SubheaderRight>
      </Subheader> */}
      <Container>
        <Card className="h-full">
          <CardHeader>
            <CardHeaderChild>
              <CardTitle>Table</CardTitle>
            </CardHeaderChild>
            {/* <CardHeaderChild>
              <Button icon="HeroLink" color="zinc" variant="outline">
                Click
              </Button>
              <Button icon="HeroCloudArrowDown" variant="solid">
                Click
              </Button>
            </CardHeaderChild> */}
          </CardHeader>
          <CardBody className="overflow-auto">
            <TableTemplate className="table-fixed max-md:min-w-[70rem]" table={table} />
          </CardBody>
          <TableCardFooterTemplate table={table} />
        </Card>
      </Container>
    </PageWrapper>
  );
};

export default RoleListPage;
