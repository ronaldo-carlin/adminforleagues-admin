import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useFormik } from 'formik';
// Components
import PageWrapper from '../../../../components/layouts/PageWrapper/PageWrapper';
import Container from '../../../../components/layouts/Container/Container';
import Card, { CardBody, CardHeader, CardHeaderChild, CardTitle } from '../../../../components/ui/Card';
import Label from '../../../../components/form/Label';
import Input from '../../../../components/form/Input';
import Checkbox from '../../../../components/form/Checkbox';
import Button from '../../../../components/ui/Button';
import Badge from '../../../../components/ui/Badge';
import Subheader, { SubheaderLeft, SubheaderRight, SubheaderSeparator } from '../../../../components/layouts/Subheader/Subheader';
import { appPages } from '../../../../config/pages.config';
import useSaveBtn from '../../../../hooks/useSaveBtn';
// Types
import { TAccessControls } from '../../../../types/access_control';
// Context
import AccessControlContext from '../../../../context/accessControlContext';
import { useAuth } from '../../../../context/authContext';

const RolePage = () => {
  // Obtiene el guid de los parámetros de la URL
  const { guid } = useParams();
  // Obtiene el contexto de los accesos
  const { accessControlByRol, fetchAccessControlByRol, accessControls, modules, fetchModules, permissions, fetchPermissions } =
    useContext(AccessControlContext);
  // estado para saber si se está guardando la información
  const [isSaving, setIsSaving] = useState<boolean>(false);
  // Obtiene el contexto del token de autenticación
  const { getToken } = useAuth();
  // estado para saber si se está cargando la información
  const [isLoading, setIsLoading] = useState<boolean>(false);
  // estado para saber la información del control de acceso

  useEffect(() => {
    // Función para obtener los módulos
    const fetchModule = async () => {
      setIsLoading(true); // Marca como cargando
      // validamos si ya se cargaron los módulos tomando en cuenta que es un arreglo
      if (modules.length) {
        setIsLoading(false); // Marca como no cargando
        return;
      } // Evita solicitudes duplicadas
      const token = await getToken(); // Espera a que el token esté disponible
      if (token && fetchModules) {
        await fetchModules(); // Obtiene los módulos
      }
      setIsLoading(false); // Marca como no cargando
    };

    fetchModule(); // Llama a la función fetchModule

    // Función para obtener los permisos
    const fetchAllPermissions = async () => {
      setIsLoading(true);
      const token = await getToken();
      if (token && fetchPermissions) {
        await fetchPermissions();
      }
      setIsLoading(false);
    };

    fetchAllPermissions(); // Llama a la función fetchPermissions
  }, [getToken]); // Incluye isLoading como dependencia

  useEffect(() => {
    const fetchAccessControlByRolfunc = async () => {
      setIsLoading(true);
      const role = accessControls.find((item) => item.guid === guid);

      // Si no se encuentra el rol y accessControls está vacío, se llama a fetchAccessControlByRol
      if (!role && accessControls.length === 0) {
        const token = await getToken();
        if (token && fetchAccessControlByRol) {
          await fetchAccessControlByRol(guid);
        }
        setIsLoading(false);
      } else if (role) {
        setIsLoading(false);
      }

      setIsLoading(false);
    };

    fetchAccessControlByRolfunc();
  }, [guid, accessControls, getToken]);

  // Intenta encontrar el control de acceso
  const accessControlItem = accessControls.find((item) => item.guid === guid);

  // Si no se encuentra el control de acceso, utiliza el primer elemento de accessControlByRol
  const fallbackAccessControl = accessControlByRol?.[0] || { guid: '', role: { name: '' } };

  const formik = useFormik({
    initialValues: {
      guid: accessControlItem?.guid || fallbackAccessControl.guid, // Asigna el guid encontrado o el de accessControlByRol
      name: accessControlItem?.role?.name || fallbackAccessControl.role.name, // Asigna el nombre del rol encontrado o el de accessControlByRol
      modules: accessControls.length
        ? accessControls.reduce((acc, item) => {
            // Verifica si item.modules está definido y es un array
            if (item.modules && Array.isArray(item.modules)) {
              item.modules.forEach((module) => {
                // Verifica si el módulo tiene permisos
                if (module.permissions && Array.isArray(module.permissions)) {
                  acc[module.guid] = module.permissions.reduce((permAcc, permission) => {
                    permAcc[permission.guid] = true; // O ajusta según la lógica que necesites
                    return permAcc;
                  }, {});
                }
              });
            }
            return acc;
          }, {})
        : accessControlByRol.length
          ? accessControlByRol[0].modules.reduce((acc, item) => {
              if (item.permissions && Array.isArray(item.permissions)) {
                acc[item.guid] = item.permissions.reduce((permAcc, permission) => {
                  permAcc[permission.guid] = true; // O ajusta según la lógica que necesites
                  return permAcc;
                }, {});
              }
              return acc;
            }, {})
          : {},
    },
    onSubmit: () => {
      // Aquí manejarás la lógica del submit
    },
  });

  const { saveBtnText, saveBtnColor, saveBtnDisable } = useSaveBtn({
    isNewItem: false,
    isSaving,
    isDirty: formik.dirty,
  });

  return (
    <PageWrapper name={formik.values.name}>
      <Subheader>
        <SubheaderLeft>
          <Link to={`../${appPages.crmAppPages.subPages.rolePage.subPages.listPage.to}`}>
            <Button icon="HeroArrowLeft" className="!px-0">
              Volver a la lista
            </Button>
          </Link>
          <SubheaderSeparator />
          <Badge color="blue" variant="outline" rounded="rounded-full" className="border-transparent">
            Editar rol {accessControls.length ? accessControls.find((item) => item.guid === guid)?.role.name : accessControlByRol[0]?.role.name}
          </Badge>
        </SubheaderLeft>
        <SubheaderRight>
          <Button icon="HeroServer" variant="solid" color={saveBtnColor} isDisable={saveBtnDisable} onClick={() => formik.handleSubmit()}>
            {saveBtnText}
          </Button>
        </SubheaderRight>
      </Subheader>
      <Container>
        <div className="grid grid-cols-12 gap-4">
          <div className="col-span-12">
            <h1 className="my-4 font-bold">Editar rol</h1>
          </div>
          <div className="col-span-12">
            <Card>
              <CardHeader>
                <CardHeaderChild>
                  <CardTitle>Información general</CardTitle>
                </CardHeaderChild>
              </CardHeader>
              <CardBody>
                <div className="grid grid-cols-12 gap-4">
                  <div className="col-span-12 lg:col-span-6 hidden">
                    <Label htmlFor="guid">guid</Label>
                    <Input id="guid" name="guid" onChange={formik.handleChange} value={formik?.values?.guid} disabled />
                  </div>
                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="name">name</Label>
                    <Input id="name" name="name" onChange={formik.handleChange} value={formik?.values?.name} />
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
          <div className="col-span-12">
            <div className="grid grid-cols-12 gap-4">
              {modules?.length > 0 &&
                modules?.map((module) => {
                  const modulePermissions = formik.values.modules[module.guid] || {};
                  const isReadChecked = modulePermissions['read'];

                  return (
                    <Card className="col-span-12 lg:col-span-6 bg-black p-4" key={module.guid}>
                      <CardHeader>
                        <CardHeaderChild>
                          <CardTitle>{module.name}</CardTitle>
                        </CardHeaderChild>
                      </CardHeader>
                      <CardBody className="grid grid-cols-12 gap-4">
                        {permissions?.length > 0 &&
                          permissions?.map((permission) => {
                            const isPermissionChecked = formik.values?.modules?.[module.guid]?.[permission.guid] || false;

                            return (
                              <Checkbox
                                className="col-span-3"
                                variant="switch"
                                key={permission.guid}
                                id={`${module?.guid}-${permission.guid}`}
                                name={permission.guid}
                                label={permission.name}
                                onChange={() => {
                                  const newValues = {
                                    ...formik.values?.modules?.[module.guid],
                                    [permission.guid]: !isPermissionChecked,
                                  };
                                  formik.setFieldValue('modules', {
                                    ...formik.values?.modules,
                                    [module.guid]: newValues,
                                  });
                                }}
                                // Estado del checkbox basado solo en formik
                                checked={isPermissionChecked}
                                // Deshabilitar si no es 'read' y 'read' no está activado
                                disabled={permission.name !== 'read' && !isReadChecked}
                              />
                            );
                          })}
                      </CardBody>
                    </Card>
                  );
                })}
            </div>
          </div>
        </div>
      </Container>
    </PageWrapper>
  );
};

export default RolePage;
