export type TTypes_competitions = {
  id: number;
  guid: string;
  name: string;
  description: string;
  is_active: boolean;
};

export type UpdateTypesCompetitions = {
  id: string;
  guid: string;
  name: string;
  description: string;
};
