export type TPlayer = {
  id: string;
  guid: string;
  age: number;
  birthdate: string;
  city: string;
  country: string;
  dorsal_number: number;
  facebook: string;
  first_name: string;
  height: number;
  image_path: string;
  instagram: string;
  last_name: string;
  nacionality: string;
  personality_traits: string;
  phone_number: string;
  position: string;
  skilled_foot: string;
  state: string;
  team_guid: string;
  weight: number;
  team: {
    guid: string;
    name: string;
    competition: {
      guid: string;
      category_guid: string;
      season_guid: string;
      sport_guid: string;
      type_competition_guid: string;
      name: string;
      description: string;
      logo_path: string;
      teams: number;
      players: number;
      players_field: number;
      start_date: string;
      end_date: string;
      site_web: string;
      is_active: boolean;
      category: {
        guid: string;
        name: string;
        description: string;
        is_active: boolean;
      };
      season: {
        guid: string;
        name: string;
        description: string;
        start_date: string;
        end_date: string;
        is_active: boolean;
      };
      sport: {
        guid: string;
        name: string;
        description: string;
        is_active: boolean;
      };
      type_competition: {
        guid: string;
        name: string;
        description: string;
        is_active: boolean;
      };
    };
    is_active: boolean;
    actions: string;
    exportable: boolean;
  };
};

export type AuthResponse = {
  status: string;
  message: string;
  code: number;
};

export type RegisterInput = {
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  role_guid: string;
  phone: string;
};

export type UpdatePlayer = {
  guid: string;
  age: number;
  birthdate: string;
  city: string;
  country: string;
  dorsal_number: number;
  facebook: string;
  first_name: string;
  height: number;
  image_path: string;
  instagram: string;
  last_name: string;
  nacionality: string;
  personality_traits: string;
  phone_number: string;
  position: string;
  skilled_foot: string;
  state: string;
  team_guid: string;
  weight: number;
  is_active: boolean;
};
