export type TSport = {
  id: number;
  guid: string;
  name: string;
  description: string;
  logo_path: string;
  teams: number;
  players: number | null;
  players_field: number;
  start_date: string;
  end_date: string;
  site_web: string;
  is_active: boolean;
  category: {
    guid: string;
    name: string;
    sport: {
      guid: string;
      name: string;
    };
  };
  season: {
    guid: string;
    name: string;
  };
  sport: {
    guid: string;
    name: string;
  };
  type_competition: {
    guid: string;
    name: string;
  };
};

export type UpdateSport = {
  id: string;
  guid: string;
  category_guid: string;
  season_guid: string;
  sport_guid: string;
  type_competition_guid: string;
  name: string;
  description: string;
  logo_path: string;
  teams: number;
  players: string;
  players_field: number;
  start_date: string;
  end_date: string;
  site_web: string;
};
