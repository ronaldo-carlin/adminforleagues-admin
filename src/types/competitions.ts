export type TCompetition = {
  id: string;
  guid: string;
  name: string;
  description: string;
  logo_path: string;
  teams: number;
  players: null;
  players_field: number;
  start_date: string;
  end_date: string;
  site_web: string;
  is_active: boolean;
  actions: string;
  category: {
    guid: string;
    name: string;
    sport: {
      guid: string;
      name: string;
    };
  };
  season: {
    guid: string;
    name: string;
  };
  sport: {
    guid: string;
    name: string;
  };
  type_competition: {
    guid: string;
    name: string;
  };
};

export type UpdateCompetition = {
  id: string;
  guid: string;
  name: string;
  description: string;
  logo_path: string;
  teams: number;
  players: null;
  players_field: number;
  start_date: string;
  end_date: string;
  site_web: string;
  is_active: boolean;
  actions: string;
  category: {
    guid: string;
    name: string;
    sport: {
      guid: string;
      name: string;
    };
  };
  season: {
    guid: string;
    name: string;
  };
  sport: {
    guid: string;
    name: string;
  };
  type_competition: {
    guid: string;
    name: string;
  };
};
