export type TUser = {
  updated_at: any;
  code: number;
  user: any;
  id: string;
  guid: string;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  password: string;
  position: string;
  role_guid: string;
  role: {
    name: string;
  };
  isVerified: true;
  image_path: string;
  picture: string;
  image: {
    org: 'url_a_la_imagen_original';
    thumb: 'https://lh3.googleusercontent.com/a/ACg8ocLW3eDzZCs_yET4v8V7uLRQBGw4WtCXCP8v2KVMf2tW=s96-c';
  };
  google_id: string;
  facebook_id: string;
  google: string;
  facebook: string;
  instagram: string;
  socialAuth: {
    google: true;
    facebook: false;
    apple: false;
  };
  socialProfiles: {
    twitter: '@alexronaldo';
    facebook: 'alexronaldoFB';
    instagram: 'alexronaldoIG';
    github: 'alexronaldoGH';
  };
  twoFactorAuth: false;
  newsletter: {
    weeklyNewsletter: true;
    lifecycleEmails: false;
    promotionalEmails: true;
    productUpdates: true;
  };
  bio: 'A passionate developer.';
  country: string;
  state: string;
  city: string;
  weeklyNewsletter: boolean;
  lifecycleEmails: boolean;
  promotionalEmails: boolean;
  productUpdates: boolean;
};

export type AuthResponse = {
  status: string;
  message: string;
  code: number;
};

export type RegisterInput = {
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  role_guid: string;
  phone: string;
};

export type UpdateUser = {
  guid: string;
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  role_guid: string;
  country: string;
  state: string;
  city: string;
  facebook: string;
  instagram: string;
  twoFactorAuth: boolean;
  weeklyNewsletter: boolean;
  lifecycleEmails: boolean;
  promotionalEmails: boolean;
  productUpdates: boolean;
};
