export type TPermissions = {
  id: number;
  guid: string;
  name: string;
  description: string;
};
