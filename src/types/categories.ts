export type TCategories = {
  id: number;
  guid: string;
  name: string;
  description: string;
  logo_path: string;
  color: string;
  age_min: number;
  age_max: number;
  gender: string;
  level: string;
  sport: {
    guid: string;
    name: string;
  };
  is_active: boolean;
};

export type UpdateCategories = {
  id: number;
  guid: string;
  sport_guid: string;
  name: string;
  description: string;
  logo_path: string;
  color: string;
  age_min: number;
  age_max: number;
  gender: string;
  level: string;
  is_active: boolean;
};
