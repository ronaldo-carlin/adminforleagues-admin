export type TTeam = {
  id: string;
  guid: string;
  name: string;
  competition_guid: string;
  group: string;
  image_path: string;
  description: string;
  is_active: boolean;
  actions: string;
};

export type AuthResponse = {
  status: string;
  message: string;
  code: number;
};

export type RegisterInput = {
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  role_guid: string;
  phone: string;
};

export type UpdateTeam = {
  guid: string;
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  role_guid: string;
  country: string;
  state: string;
  city: string;
  facebook: string;
  instagram: string;
  twoFactorAuth: boolean;
  weeklyNewsletter: boolean;
  lifecycleEmails: boolean;
  promotionalEmails: boolean;
  productUpdates: boolean;
};
