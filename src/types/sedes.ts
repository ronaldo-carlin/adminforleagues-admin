export type TSede = {
  id: string;
  guid: string;
  name: string;
  description: string;
  schedule: string;
  address: string;
  city: string;
  state: string;
  country: string;
  capacity: string;
  latitude: string;
  longitude: string;
  image_path: string;
  is_active: boolean;
  actions: string;
};

export type UpdateSede = {
  id: string;
  guid: string;
  name: string;
  description: string;
  schedule: string;
  address: string;
  city: string;
  state: string;
  country: string;
  capacity: string;
  latitude: string;
  longitude: string;
  image_path: string;
  is_active: boolean;
  actions: string;
};
