import { TPermissions } from './permissions';

export type TModules = {
  guid: string;
  name: string;
  permissions: [TPermissions];
};
