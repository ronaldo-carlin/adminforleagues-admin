import { T } from '@fullcalendar/core/internal-common';
import { TModules } from './modules';

/* creamos los types para el modulo de access_control */
export type TAccessControls = {
  guid: string;
  is_active: boolean;
  role: {
    guid: string;
    name: string;
  };
  modules: [TModules];
};

export type TAccessControl = {
  guid: string;
};

export type TCreateAccessControl = {
  role_guid: string;
  module_guid: string;
  permissions_guid: string;
};

export type TUpdateAccessControl = {
  guid: string;
  role_guid: string;
  module_guid: string;
  permissions_guid: string;
};

export type TDeteleAccessControl = {
  guid: string;
};

export type TAccessControlByRole = {
  guid: string;
};
