export type TypeApiResponse = {
  status: string;
  message: string;
  code: number;
  data: any;
};
