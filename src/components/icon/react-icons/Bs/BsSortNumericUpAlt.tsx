// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { BsSortNumericUpAlt } from "react-icons/bs";

const ReactBsSortNumericUpAlt = (props: SVGProps<SVGSVGElement>) => {
    return <BsSortNumericUpAlt  {...props} />;
};

export default ReactBsSortNumericUpAlt;
