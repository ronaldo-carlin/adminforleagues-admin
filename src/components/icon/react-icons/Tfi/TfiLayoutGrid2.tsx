// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { TfiLayoutGrid2 } from "react-icons/tfi";

const ReactTfiLayoutGrid2 = (props: SVGProps<SVGSVGElement>) => {
    return <TfiLayoutGrid2  {...props} />;
};

export default ReactTfiLayoutGrid2;
