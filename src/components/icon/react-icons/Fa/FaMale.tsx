// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { FaMale } from "react-icons/fa";

const ReactFaMale = (props: SVGProps<SVGSVGElement>) => {
    return <FaMale  {...props} />;
};

export default ReactFaMale;
