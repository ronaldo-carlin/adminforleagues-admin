// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { FaFemale } from "react-icons/fa";

const ReactFaFemale = (props: SVGProps<SVGSVGElement>) => {
    return <FaFemale  {...props} />;
};

export default ReactFaFemale;
