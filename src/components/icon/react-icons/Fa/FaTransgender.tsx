// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { FaTransgender } from "react-icons/fa";

const ReactFaTransgender = (props: SVGProps<SVGSVGElement>) => {
    return <FaTransgender  {...props} />;
};

export default ReactFaTransgender;
