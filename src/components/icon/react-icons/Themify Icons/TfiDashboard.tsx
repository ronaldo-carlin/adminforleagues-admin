// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { TfiDashboard } from 'react-icons/tfi';

const ReactTfiDashboard = (props: SVGProps<SVGSVGElement>) => {
	return <TfiDashboard {...props} />;
};

export default ReactTfiDashboard;
