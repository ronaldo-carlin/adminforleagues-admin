// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { TbPlayVolleyball } from "react-icons/tb";

const ReactTbPlayVolleyball = (props: SVGProps<SVGSVGElement>) => {
    return <TbPlayVolleyball {...props} />;
};

export default ReactTbPlayVolleyball;
