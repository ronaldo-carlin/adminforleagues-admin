// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiTennisRacket } from "react-icons/gi";

const ReactGiTennisRacket = (props: SVGProps<SVGSVGElement>) => {
    return <GiTennisRacket {...props} />;
};

export default ReactGiTennisRacket;
