// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiBasketballBasket } from "react-icons/gi";

const ReactGiBasketballBasket = (props: SVGProps<SVGSVGElement>) => {
    return <GiBasketballBasket {...props} />;
};

export default ReactGiBasketballBasket;
