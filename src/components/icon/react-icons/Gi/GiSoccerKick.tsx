// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiSoccerKick } from "react-icons/gi";

const ReactGiSoccerKick = (props: SVGProps<SVGSVGElement>) => {
    return <GiSoccerKick {...props} />;
};

export default ReactGiSoccerKick;
