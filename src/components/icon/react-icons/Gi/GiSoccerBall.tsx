// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiSoccerBall } from "react-icons/gi";

const ReactGiSoccerBall = (props: SVGProps<SVGSVGElement>) => {
    return <GiSoccerBall {...props} />;
};

export default ReactGiSoccerBall;
