// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiSoccerField } from "react-icons/gi";

const ReactGiSoccerField = (props: SVGProps<SVGSVGElement>) => {
    return <GiSoccerField {...props} />;
};

export default ReactGiSoccerField;
