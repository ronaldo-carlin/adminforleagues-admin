// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { GiAmericanFootballPlayer } from "react-icons/gi";

const ReactGiAmericanFootballPlayer = (props: SVGProps<SVGSVGElement>) => {
    return <GiAmericanFootballPlayer {...props} />;
};

export default ReactGiAmericanFootballPlayer;
