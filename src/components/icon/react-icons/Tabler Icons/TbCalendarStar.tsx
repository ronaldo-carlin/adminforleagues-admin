// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { TbCalendarStar } from 'react-icons/tb';

const ReactTbCalendarStar = (props: SVGProps<SVGSVGElement>) => {
	return <TbCalendarStar {...props} />;
};

export default ReactTbCalendarStar;
