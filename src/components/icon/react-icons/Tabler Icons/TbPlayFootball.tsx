// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { TbPlayFootball } from 'react-icons/tb';

const ReactTbPlayFootball = (props: SVGProps<SVGSVGElement>) => {
	return <TbPlayFootball {...props} />;
};

export default ReactTbPlayFootball;
