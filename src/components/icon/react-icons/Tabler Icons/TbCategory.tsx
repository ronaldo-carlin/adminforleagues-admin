// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { TbCategory } from 'react-icons/tb';

const ReactTbCategory = (props: SVGProps<SVGSVGElement>) => {
	return <TbCategory {...props} />;
};

export default ReactTbCategory;
