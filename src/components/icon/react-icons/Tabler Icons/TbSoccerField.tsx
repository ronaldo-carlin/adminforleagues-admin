// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { TbSoccerField } from 'react-icons/tb';

const ReactTbSoccerField = (props: SVGProps<SVGSVGElement>) => {
	return <TbSoccerField {...props} />;
};

export default ReactTbSoccerField;
