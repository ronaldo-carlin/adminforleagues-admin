// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { LiaFutbol } from 'react-icons/lia'; // Importa todos los iconos de react-icons

const ReactLiaFutbol = (props: SVGProps<SVGSVGElement>) => {
	return <LiaFutbol {...props} />;
};

export default ReactLiaFutbol;
