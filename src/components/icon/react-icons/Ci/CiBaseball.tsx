// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { CiBaseball } from "react-icons/ci";

const ReactCiBaseball = (props: SVGProps<SVGSVGElement>) => {
    return <CiBaseball {...props} />;
};

export default ReactCiBaseball;
