// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
// Heroicons 2
export { default as ReactHiOutlineTrophy } from './Heroicons 2/HiOutlineTrophy';
// Icons8 Line Awesome
export { default as ReactLiaFutbol } from './Icons8 Line Awesome/LiaFutbol';
// Tabler Icons
export { default as ReactTbCalendarStar } from './Tabler Icons/TbCalendarStar';
export { default as ReactTbCategory } from './Tabler Icons/TbCategory';
export { default as ReactTbPlayFootball } from './Tabler Icons/TbPlayFootball';
export { default as ReactTbTournament } from './Tabler Icons/TbTournament';
export { default as ReactTbSoccerField } from './Tabler Icons/TbSoccerField';
// Themify Icons
export { default as ReactTfiDashboard } from './Themify Icons/TfiDashboard';
// Game Icons
export { default as ReactGiWhistle } from './Game Icons/GiWhistle';
export { default as ReactGiAbstract042 } from './Game Icons/GiAbstract042';
// Gi
export { default as ReactGiBasketballBasket } from './Gi/GiBasketballBasket';
export { default as ReactGiSoccerField } from './Gi/GiSoccerField';
export { default as ReactGiSoccerKick } from './Gi/GiSoccerKick';
export { default as ReactGiSoccerBall } from './Gi/GiSoccerBall';
export { default as ReactGiTennisRacket } from './Gi/GiTennisRacket';
export { default as ReactGiAmericanFootballPlayer } from './Gi/GiAmericanFootballPlayer';
// Tb
export { default as ReactTbPlayVolleyball } from './Tb/TbPlayVolleyball';
// Ci
export { default as ReactCiBaseball } from './Ci/CiBaseball';
// Hi2
export { default as ReactMdFormatListNumbered } from './Md/MdFormatListNumbered';
// Md
export { default as ReactMdAdd } from './Md/MdAdd';
// Bs
export { default as ReactBsSortNumericUpAlt } from './Bs/BsSortNumericUpAlt';
// Tfi
export { default as ReactTfiLayoutGrid2 } from './Tfi/TfiLayoutGrid2';
// Fa
export { default as ReactFaFemale } from './Fa/fafemale';
export { default as ReactFaMale } from './Fa/FaMale';
export { default as ReactFaTransgender } from './Fa/FaTransgender';