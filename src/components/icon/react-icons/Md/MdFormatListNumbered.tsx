// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { MdFormatListNumbered } from "react-icons/md";

const ReactMdFormatListNumbered = (props: SVGProps<SVGSVGElement>) => {
    return <MdFormatListNumbered  {...props} />;
};

export default ReactMdFormatListNumbered;
