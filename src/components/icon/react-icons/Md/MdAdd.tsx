// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import React from 'react';
import { MdAdd } from "react-icons/md";

const ReactMdAdd = (props: SVGProps<SVGSVGElement>) => {
    return <MdAdd  {...props} />;
};

export default ReactMdAdd;
