// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { HiOutlineTrophy } from 'react-icons/hi2';

const ReactHiOutlineTrophy = (props: SVGProps<SVGSVGElement>) => {
	return <HiOutlineTrophy {...props} />;
};

export default ReactHiOutlineTrophy;
