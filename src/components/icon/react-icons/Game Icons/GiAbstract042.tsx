// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { GiAbstract042 } from 'react-icons/gi';

const ReactGiAbstract042 = (props: SVGProps<SVGSVGElement>) => {
	return <GiAbstract042 {...props} />;
};

export default ReactGiAbstract042;
