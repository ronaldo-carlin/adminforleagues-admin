// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
import { GiWhistle } from 'react-icons/gi';

const ReactGiWhistle = (props: SVGProps<SVGSVGElement>) => {
	return <GiWhistle {...props} />;
};

export default ReactGiWhistle;
