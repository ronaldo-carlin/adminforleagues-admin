import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup'; // Importa Yup
// mocks
import rolesDb from '../../mocks/db/roles.db';
// components
import Card, { CardBody, CardFooter, CardFooterChild } from '../ui/Card';
import Button from '../ui/Button';
import Label from './Label';
import Input from './Input';
import Select from './Select';
import Icon from '../icon/Icon';
import { useValidation } from '../../utils/validation.utils'; // Importa las funciones de validación
import { useCompetitionContext } from '../../context/competitionContext';
import { competitionClient } from '../../services/competitions';
import { useToast } from '../../context/ToastContext'; // Importa el contexto de toast

interface FormAddCompetitionProps {
  onSuccess?: () => void;
}

const FormAddCompetition: React.FC<FormAddCompetitionProps> = ({ onSuccess }) => {
  const { validateEmailUniquenes, validatePhoneUniquenes, validateUsernameUniquenes } = useValidation();
  const { competitions, fetchCompetitions, setModalAdd } = useCompetitionContext(); // Obtén los usuarios y la función para cargarlos
  //creamos un estado para cuando se envia el formulario y se esta procesando este en true y cuando se termina de enviar el formulario este cambia a false
  const [loading, setLoading] = useState(false);
  const { showToast } = useToast() as { showToast: (message: string, type: string) => void }; // Obtén la función showToast del contexto de Toast

  // Usa useEffect para cargar usuarios si no están cargados
  useEffect(() => {
    if (competitions.length === 0) {
      fetchCompetitions(); // Si los usuarios no están cargados, llama a la función para cargarlos
    }
  }, [competitions, fetchCompetitions]);

  // Define el esquema de validación con Yup
  const validationSchema = Yup.object({
    username: Yup.string()
      .min(3, 'El nombre de usuario debe tener al menos 3 caracteres')
      .max(50, 'El nombre de usuario debe tener máximo 50 caracteres')
      .required('El nombre de usuario es obligatorio')
      .matches(/^[a-zA-Z0-9_]+$/, 'El nombre de usuario solo puede contener letras, números y guiones bajos')
      .test('username-unique', 'El nombre de usuario ya está en uso', async function (value) {
        try {
          // Asegúrate de que los usuarios estén cargados antes de validar
          if (competitions.length === 0) {
            await fetchCompetitions();
          }

          const isUnique = await validateUsernameUniquenes(value, '');
          return isUnique;
        } catch (error) {
          return this.createError({ message: error.message });
        }
      }),
    email: Yup.string()
      .email('Debes ingresar un correo válido')
      .required('El correo es obligatorio')
      .test('email-unique', 'El correo ya está en uso', async function (value) {
        try {
          // Asegúrate de que los usuarios estén cargados antes de validar
          if (competitions.length === 0) {
            await fetchCompetitions();
          }

          const isUnique = await validateEmailUniquenes(value, '');
          return isUnique;
        } catch (error) {
          return this.createError({ message: error.message });
        }
      }),
    phone: Yup.string()
      .matches(/^[0-9]+$/, 'El teléfono solo debe contener números')
      .min(10, 'El teléfono debe tener al menos 10 dígitos')
      .max(10, 'El teléfono debe tener máximo 10 dígitos')
      .required('El teléfono es obligatorio')
      .test('phone-unique', 'El teléfono ya está en uso', async function (value) {
        try {
          // Asegúrate de que los usuarios estén cargados antes de validar
          if (competitions.length === 0) {
            await fetchCompetitions();
          }

          const isUnique = await validatePhoneUniquenes(value, '');
          return isUnique;
        } catch (error) {
          return this.createError({ message: error.message });
        }
      }),
    first_name: Yup.string().required('El nombre es obligatorio'),
    last_name: Yup.string().required('El apellido es obligatorio'),
    role_guid: Yup.string().required('Debes seleccionar un rol'),
  });

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      phone: '',
      first_name: '',
      last_name: '',
      role_guid: '',
    },
    validationSchema, // Asocia el esquema de validación al formulario
    onSubmit: async (values) => {
      try {
        setLoading(true); // Establece el estado de carga a true cuando se envíe el formulario
        // Api para guardar el usuario
        const response = await competitionClient.AddCompetition(values);

        if (response.code !== 201) {
          showToast('Error al guardar usuario', 'error'); // Muestra un toast de error
          throw new Error('Error al guardar usuario');
        }
        // Resetea el formulario y marca como "no sucio"
        formik.resetForm({ values });
        //agergamos un toast de exito para que se muestre cuando se envia el correo
        showToast('¡Usuario guardado con éxito!', 'success'); // Muestra un toast de error'
        // Recarga los usuarios para mostrar el nuevo usuario
        fetchCompetitions();
        // Cierra el modal de agregar usuario
        setModalAdd(false);
        // Establece el estado de carga a false cuando se haya enviado el formulario
        setLoading(false);
      } catch (error) {
        console.error('Error al guardar usuario:', error);
      }
    },
    enableReinitialize: true,
  });

  return (
    <>
      <div className="flex h-full flex-wrap content-start">
        <div className="mb-4 grid w-full grid-cols-12 gap-4">
          <div className="col-span-12 flex flex-col gap-4">
            <Card>
              <CardBody>
                <div className="grid grid-cols-12 gap-4">
                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="first_name">Nombre</Label>
                    <Input
                      id="first_name"
                      name="first_name"
                      onChange={formik.handleChange}
                      value={formik.values.first_name}
                      autoComplete="given-name"
                      autoCapitalize="words"
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.first_name && formik.errors.first_name ? <div className="text-red-500">{formik.errors.first_name}</div> : null}
                  </div>
                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="last_name">Apellido</Label>
                    <Input
                      id="last_name"
                      name="last_name"
                      onChange={formik.handleChange}
                      value={formik.values.last_name}
                      autoComplete="family-name"
                      autoCapitalize="words"
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.last_name && formik.errors.last_name ? <div className="text-red-500">{formik.errors.last_name}</div> : null}
                  </div>

                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="username">Competitionname</Label>
                    <Input
                      id="username"
                      name="username"
                      onChange={formik.handleChange}
                      value={formik.values.username}
                      autoComplete="username"
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.username && formik.errors.username ? <div className="text-red-500">{formik.errors.username}</div> : null}
                  </div>
                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="email">Correo</Label>
                    <Input
                      type="email"
                      id="email"
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      autoComplete="email"
                    />
                    {formik.touched.email && formik.errors.email ? <div className="text-red-500">{formik.errors.email}</div> : null}
                  </div>
                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="phone">Teléfono</Label>
                    <Input
                      id="phone"
                      name="phone"
                      onChange={formik.handleChange}
                      value={formik.values.phone}
                      autoComplete="tel"
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.phone && formik.errors.phone ? <div className="text-red-500">{formik.errors.phone}</div> : null}
                  </div>

                  <div className="col-span-12 lg:col-span-6">
                    <Label htmlFor="role_guid">Rol</Label>
                    <Select
                      name="role_guid"
                      onChange={formik.handleChange}
                      value={formik.values.role_guid}
                      placeholder="Selecciona un rol"
                      onBlur={formik.handleBlur}
                    >
                      {rolesDb.map((role) => (
                        <option key={role.id} value={role.guid}>
                          {role.name}
                        </option>
                      ))}
                    </Select>
                    {formik.touched.role_guid && formik.errors.role_guid ? <div className="text-red-500">{formik.errors.role_guid}</div> : null}
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
      <div className="flex">
        <div className="grid w-full grid-cols-12 gap-4">
          <div className="col-span-12">
            <Card>
              <CardFooter>
                <CardFooterChild>
                  {formik.dirty ? (
                    <div className="flex items-center gap-2 text-amber-500">
                      <Icon icon="HeroExclamationTriangle" size="text-2xl" />
                      <span>El formulario tiene cambios sin guardar</span>
                    </div>
                  ) : (
                    <span>Formulario sin cambios</span>
                  )}
                </CardFooterChild>
                <CardFooterChild>
                  <Button icon="HeroServer" variant="solid" color="blue" isDisable={!formik.dirty} onClick={() => formik.handleSubmit()}>
                    Agregar
                  </Button>
                </CardFooterChild>
              </CardFooter>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormAddCompetition;
