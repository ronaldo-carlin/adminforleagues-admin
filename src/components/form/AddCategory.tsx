import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup'; // Importa Yup
// mocks
import rolesDb from '../../mocks/db/roles.db';
// components
import Card, { CardBody, CardFooter, CardFooterChild } from '../ui/Card';
import Button from '../ui/Button';
import Label from './Label';
import Input from './Input';
import Select from './Select';
import Icon from '../icon/Icon';
import SelectReact from './SelectReact';
import RichText from '../RichText';
import Radio, { RadioGroup } from './Radio';
import { useValidation } from '../../utils/validation.utils'; // Importa las funciones de validación
import { useUserContext } from '../../context/userContext';
import { userClient } from '../../services/users';
import { useToast } from '../../context/ToastContext'; // Importa el contexto de toast

interface Sport {
  guid: string;
  name: string;
}

interface FormAddUserProps {
  onSuccess?: () => void;
  sport: Sport[];
}

const FormAddCategory: React.FC<FormAddUserProps> = ({ onSuccess, sport }) => {
  const { validateEmailUniquenes, validatePhoneUniquenes, validateUsernameUniquenes } = useValidation();
  const { users, fetchUsers, setModalAdd } = useUserContext(); // Obtén los usuarios y la función para cargarlos
  //creamos un estado para cuando se envia el formulario y se esta procesando este en true y cuando se termina de enviar el formulario este cambia a false
  const [loading, setLoading] = useState(false);
  const { showToast } = useToast() as { showToast: (message: string, type: string) => void }; // Obtén la función showToast del contexto de Toast

  // Usa useEffect para cargar usuarios si no están cargados
  useEffect(() => {
    if (users.length === 0) {
      fetchUsers(); // Si los usuarios no están cargados, llama a la función para cargarlos
    }
  }, [users, fetchUsers]);

  // Define el esquema de validación con Yup
  const validationSchema = Yup.object({
    sport_guid: Yup.string().required('El GUID del deporte es obligatorio'),
    name: Yup.string().required('El nombre es obligatorio'),
    description: Yup.string().required('La descripción es obligatoria'),
    logo_path: Yup.string().required('La ruta del logo es obligatoria'),
    color: Yup.string().required('El color es obligatorio'),
    age_min: Yup.number().required('La edad mínima es obligatoria'),
    age_max: Yup.number().required('La edad máxima es obligatoria'),
    gander: Yup.string().required('El género es obligatorio'),
    level: Yup.string().required('El nivel es obligatorio'),
  });

  const formik = useFormik({
    initialValues: {
      sport_guid: '',
      name: '',
      description: '',
      logo_path: '',
      color: '',
      age_min: '',
      age_max: '',
      gander: '',
      level: '',
    },
    validationSchema, // Asocia el esquema de validación al formulario
    onSubmit: async (values) => {
      try {
        setLoading(true); // Establece el estado de carga a true cuando se envíe el formulario
        // Api para guardar el usuario
        const response = await userClient.Add(values);

        if (response.code !== 201) {
          showToast('Error al guardar usuario', 'error'); // Muestra un toast de error
          throw new Error('Error al guardar usuario');
        }
        // Resetea el formulario y marca como "no sucio"
        formik.resetForm({ values });
        //agergamos un toast de exito para que se muestre cuando se envia el correo
        showToast('¡Usuario guardado con éxito!', 'success'); // Muestra un toast de error'
        // Recarga los usuarios para mostrar el nuevo usuario
        fetchUsers();
        // Cierra el modal de agregar usuario
        setModalAdd(false);
        // Establece el estado de carga a false cuando se haya enviado el formulario
        setLoading(false);
      } catch (error) {
        console.error('Error al guardar usuario:', error);
      }
    },
    enableReinitialize: true,
  });

  // opciones de género
  const genres = {
    0: { name: 'Masculino', icon: 'FaMale', guid: '0' },
    1: { name: 'Femenino', icon: 'FaFemale', guid: '1' },
    2: { name: 'Mixto', icon: 'FaTransgender', guid: '2' },
  };

  return (
    <>
      <div className="flex h-full flex-wrap content-start">
        <div className="mb-4 grid w-full grid-cols-12 gap-4">
          <div className="col-span-12 flex flex-col gap-4">
            <Card>
              <CardBody>
                <div className="grid grid-cols-12 gap-4">
                  <div className="col-span-12 lg:col-span-12">
                    <Label htmlFor="name">Nombre</Label>
                    <Input
                      id="name"
                      name="name"
                      onChange={formik.handleChange}
                      value={formik.values.name}
                      autoComplete="given-name"
                      autoCapitalize="words"
                      onBlur={formik.handleBlur}
                    />
                    {formik.touched.name && formik.errors.name ? <div className="text-red-500">{formik.errors.name}</div> : null}
                  </div>
                  <div className="col-span-12 lg:col-span-12">
                    <Label htmlFor="name">Deporte</Label>
                    <SelectReact
                      className="w-full"
                      options={sport.map((category) => ({
                        value: category.guid,
                        label: category.name,
                      }))}
                      id="sport_guid"
                      name="sport_guid"
                      value={formik.values.sport_guid}
                      onChange={formik.handleChange}
                    />
                    {formik.touched.name && formik.errors.name ? <div className="text-red-500">{formik.errors.name}</div> : null}
                  </div>
                  <div className="col-span-12 lg:col-span-12">
                    <Label htmlFor="last_name">Descripción</Label>
                    <RichText
                      id="description"
                      value={formik.values.description}
                      handleChange={(event) => {
                        formik
                          .setFieldValue('description', event)
                          .then(() => {})
                          .catch(() => {});
                      }}
                    />
                    {formik.touched.description && formik.errors.description ? <div className="text-red-500">{formik.errors.description}</div> : null}
                  </div>
                  <div className="col-span-6">
                    <Label htmlFor="username">Edad Minima</Label>
                    <Input
                      type="number"
                      name="Edad mínima"
                      value={formik.values.age_min}
                      min={5}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                    />
                    {formik.touched.age_min && formik.errors.age_min ? <div className="text-red-500">{formik.errors.age_min}</div> : null}
                  </div>
                  <div className="col-span-6">
                    <Label htmlFor="username">Edad Maxima</Label>
                    <Input
                      type="number"
                      name="Edad Maxima"
                      value={formik.values.age_min}
                      min={5}
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                    />
                    {formik.touched.age_min && formik.errors.age_min ? <div className="text-red-500">{formik.errors.age_min}</div> : null}
                  </div>
                  <div className="col-span-12">
                    <Label htmlFor="email">Genero</Label>
                    <RadioGroup isInline className="grid grid-cols-3 gap-1">
                      {Object.keys(genres).map((gander) => (
                        <Radio
                          key={gander}
                          label={genres[gander].name}
                          icon={genres[gander].icon}
                          icon_size={40}
                          name="gander"
                          value={genres[gander].guid}
                          selectedValue={formik.values.gander}
                          onChange={formik.handleChange}
                        />
                      ))}
                    </RadioGroup>
                    {formik.touched.gander && formik.errors.gander ? <div className="text-red-500">{formik.errors.gander}</div> : null}
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
      <div className="flex">
        <div className="grid w-full grid-cols-12 gap-4">
          <div className="col-span-12">
            <Card>
              <CardFooter>
                <CardFooterChild>
                  {formik.dirty ? (
                    <div className="flex items-center gap-2 text-amber-500">
                      <Icon icon="HeroExclamationTriangle" size="text-2xl" />
                      <span>El formulario tiene cambios sin guardar</span>
                    </div>
                  ) : (
                    <span>Formulario sin cambios</span>
                  )}
                </CardFooterChild>
                <CardFooterChild>
                  <Button icon="HeroServer" variant="solid" color="blue" isDisable={!formik.dirty} onClick={() => formik.handleSubmit()}>
                    Agregar
                  </Button>
                </CardFooterChild>
              </CardFooter>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormAddCategory;
