// Preloader.js
import React from 'react';

const Preloader = () => {
  return (
    <div className="preloader">
      {/* Aquí puedes agregar cualquier tipo de indicador de carga */}
      <div className="spinner">Cargando...</div>
    </div>
  );
};

export default Preloader;
