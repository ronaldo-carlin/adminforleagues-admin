import React, { FC, HTMLAttributes } from 'react';
import colors from 'tailwindcss/colors';
import themeConfig from '../../../config/theme.config';
// obtenemos la imagen del logo
import logo from '../../../assets/images/logo/logo.png';

type ILogoTemplateProps = HTMLAttributes<HTMLOrSVGElement>;
const LogoTemplate: FC<ILogoTemplateProps> = (props) => {
  const { ...rest } = props;
  return (
    <figure>
      <img src={logo} alt="logo" title="logo" className="h-28 w-28" />
      {/* <figcaption>{themeConfig.projectTitle}</figcaption> */}
    </figure>
  );
};

export default LogoTemplate;
