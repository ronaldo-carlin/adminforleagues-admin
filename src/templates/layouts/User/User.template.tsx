import React from 'react';
import Icon from '../../../components/icon/Icon';
import Badge from '../../../components/ui/Badge';
import { NavButton, NavItem, NavSeparator } from '../../../components/layouts/Navigation/Nav';
import { appPages, authPages } from '../../../config/pages.config';
import User from '../../../components/layouts/User/User';
import { useAuth } from '../../../context/authContext';

const UserTemplate = () => {
  const { isLoading, userData, onLogout } = useAuth();
  // console.log('userData:', userData);

  // hacemos una funcion para obtener la imagen del usuario
  const getImage = () => {
    if (userData && userData.user) {
      if (userData.user.google_id) {
        return userData.user.picture;
      }
      return userData.user.image_path;
    }
    return null;
  };

  return (
    <User
      isLoading={isLoading}
      name={userData?.user?.first_name || ''}
      nameSuffix={userData?.user.isVerified && <Icon icon="HeroCheckBadge" color="blue" />}
      position={userData?.user?.role.name || ''}
      src={getImage()}
      suffix={
        <Badge color="amber" variant="solid" className="text-xs font-bold">
          PRO
        </Badge>
      }
    >
      <NavSeparator />
      <NavItem {...authPages.profilePage} />
      <NavItem {...appPages.mailAppPages.subPages.inboxPages}>
        <Badge variant="solid" className="leading-none">
          3
        </Badge>
        <NavButton icon="HeroPlusCircle" title="New Mail" onClick={() => {}} />
      </NavItem>
      <NavItem text="Logout" icon="HeroArrowRightOnRectangle" onClick={() => onLogout()} />
    </User>
  );
};

export default UserTemplate;
