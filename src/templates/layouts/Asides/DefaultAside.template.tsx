import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Aside, { AsideBody, AsideFooter, AsideHead } from '../../../components/layouts/Aside/Aside';
import LogoAndAsideTogglePart from './_parts/LogoAndAsideToggle.part';
import DarkModeSwitcherPart from './_parts/DarkModeSwitcher.part';
import { adminPages, appPages, configPages, generalPages, leaguePages, teamsPages, refereePages } from '../../../config/pages.config';
import Nav, { NavButton, NavCollapse, NavItem, NavSeparator, NavTitle } from '../../../components/layouts/Navigation/Nav';
import UserTemplate from '../User/User.template';
import UserContext from '../../../context/userContext';

const DefaultAsideTemplate = () => {
  const navigate = useNavigate();
  const userContext = useContext(UserContext);

  return (
    <Aside>
      <AsideHead>
        <LogoAndAsideTogglePart />
      </AsideHead>
      <AsideBody>
        <Nav>
          <NavItem {...appPages.crmAppPages.subPages.crmDashboardPage} />

          <NavItem {...generalPages.calendarPage} />
          <NavItem {...generalPages.newsPage} />

          <NavSeparator />

          <NavTitle>Administrador General</NavTitle>
          <NavCollapse text={appPages.crmAppPages.text} to={appPages.crmAppPages.to} icon={appPages.crmAppPages.icon}>
            <NavItem {...appPages.crmAppPages.subPages.crmDashboardPage} />
            <NavCollapse
              text={appPages.crmAppPages.subPages.customerPage.text}
              to={appPages.crmAppPages.subPages.customerPage.to}
              icon={appPages.crmAppPages.subPages.customerPage.icon}
            >
              <NavItem {...appPages.crmAppPages.subPages.customerPage.subPages.listPage} />
              <NavItem {...appPages.crmAppPages.subPages.customerPage.subPages.editPage} />
            </NavCollapse>
            <NavCollapse
              text={appPages.crmAppPages.subPages.rolePage.text}
              to={appPages.crmAppPages.subPages.rolePage.to}
              icon={appPages.crmAppPages.subPages.rolePage.icon}
            >
              <NavItem {...appPages.crmAppPages.subPages.rolePage.subPages.listPage} />
              <NavItem {...appPages.crmAppPages.subPages.rolePage.subPages.editPage} />
            </NavCollapse>
          </NavCollapse>

          <NavCollapse text={appPages.salesAppPages.text} to={appPages.salesAppPages.to} icon={appPages.salesAppPages.icon}>
            <NavItem {...appPages.salesAppPages.subPages.salesDashboardPage} />
            <NavCollapse
              text={appPages.salesAppPages.subPages.productPage.text}
              to={appPages.salesAppPages.subPages.productPage.to}
              icon={appPages.salesAppPages.subPages.productPage.icon}
            >
              <NavItem {...appPages.salesAppPages.subPages.productPage.subPages.listPage} />
              <NavItem {...appPages.salesAppPages.subPages.productPage.subPages.editPage} />
            </NavCollapse>
            <NavCollapse
              text={appPages.salesAppPages.subPages.categoryPage.text}
              to={appPages.salesAppPages.subPages.categoryPage.to}
              icon={appPages.salesAppPages.subPages.categoryPage.icon}
            >
              <NavItem {...appPages.salesAppPages.subPages.categoryPage.subPages.listPage} />
              <NavItem {...appPages.salesAppPages.subPages.categoryPage.subPages.editPage} />
            </NavCollapse>
          </NavCollapse>

          <NavSeparator />

          <NavTitle>Administrador de Liga</NavTitle>
          {/* <NavItem {...leaguePages.leaguesPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                navigate(`../${appPages.crmAppPages.subPages.customerPage.to}/new`);
              }}
            />
          </NavItem> */}
          <NavItem {...leaguePages.competitionsPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                navigate(leaguePages.addCompetitionPage.to);
              }}
            />
          </NavItem>
          {/* <NavItem {...leaguePages.typescompetitionsPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                navigate(`../${appPages.crmAppPages.subPages.customerPage.to}/new`);
              }}
            />
          </NavItem> */}
          <NavItem {...teamsPages.teamsPage} />
          <NavItem {...teamsPages.playersPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                navigate(`../${appPages.crmAppPages.subPages.customerPage.to}/new`);
              }}
            />
          </NavItem>
          <NavItem {...leaguePages.sedesPage} />
          <NavItem {...leaguePages.seasonsPage} />
          <NavItem {...leaguePages.categoriesPage} />

          <NavItem {...adminPages.usersPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                // Primero redirigir a la página de usuarios
                navigate(adminPages.usersPage.to);

                // Después de un pequeño retraso, abrir el modal
                setTimeout(() => {
                  userContext?.setModalAdd(true);
                }, 500); // Ajusta el tiempo si es necesario
              }}
            />
          </NavItem>

          <NavSeparator />

          <NavTitle>Entrenador / Dueño del equipo</NavTitle>

          <NavItem {...teamsPages.teamsPage} />
          <NavItem {...teamsPages.playersPage}>
            <NavButton
              title="New Customer"
              icon="HeroPlusCircle"
              onClick={() => {
                navigate(`../${appPages.crmAppPages.subPages.customerPage.to}/new`);
              }}
            />
          </NavItem>
          <NavItem {...teamsPages.matchesPage} />

          <NavSeparator />

          <NavTitle>Árbitro</NavTitle>
          <NavItem {...refereePages.matchesPage} />

          <NavSeparator />

          <NavTitle>Configuraciones</NavTitle>

          <NavItem {...configPages.personalizationPage} />
          <NavItem {...configPages.privacyPolicyPage} />
          <NavItem {...configPages.helpPage} />
        </Nav>
      </AsideBody>
      <AsideFooter>
        <UserTemplate />
        <DarkModeSwitcherPart />
      </AsideFooter>
    </Aside>
  );
};

export default DefaultAsideTemplate;
